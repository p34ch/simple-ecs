# OpenGL Game

![image](https://gitlab.com/p34ch-main/Game/-/wikis/uploads/f212fc104216f4a1ae2900b6ccfa0a54/image.png)

## Info

* Game modes
  * Game Debug mode with ImGUI/Profiler
  * Game Release mode with optimizations and ImGUI/Profiler
  * Game Final build

## Build

### Requirements

#### Engine

* cmake >= 3.20
* clang 19.1.0/cl 19.41.34120 x64

#### Commands

```bash
cmake --preset clang
cmake --build --preset clang-Debug

#run
cd build/bin && ./Release/SuperGame.exe
```

Or you can open this project in the VSCode with plugins to build and run. There are options to run debugger for editor and attach to the cpp process to debug cpp code.

VS also can compile and run engine part.

## SAST Tools

[PVS-Studio](https://pvs-studio.com/en/pvs-studio/?utm_source=website&utm_medium=github&utm_campaign=open_source) - static analyzer for C, C++, C#, and Java code.
