#pragma once

template<typename Func>
struct DeferredCall final : NoCopyNoMove {
    DeferredCall(Func&& f) : m_func(std::forward<Func>(f)) {}
    DeferredCall(DeferredCall&& other) noexcept : m_func(std::move(other.m_func)), m_owner(other.m_owner) {
        other.m_owner = false;
    }
    ~DeferredCall() { execute(); }

    bool cancel() noexcept {
        bool b_was_owner = m_owner;
        m_owner          = false;
        return b_was_owner;
    }

    bool execute() {
        const auto b_was_owner = m_owner;

        if (m_owner) {
            m_owner = false;
            m_func();
        }

        return b_was_owner;
    }

private:
    Func m_func;
    bool m_owner = true;
};
