#pragma once

#include <tracy/TracyOpenGL.hpp>


struct OpenGLObject {
    OpenGLObject()                   = default;
    virtual ~OpenGLObject() noexcept = default;

    OpenGLObject(const OpenGLObject& other) = default;
    OpenGLObject(OpenGLObject&& other) noexcept : m_id(std::exchange(other.m_id, 0)) {}

    OpenGLObject& operator=(const OpenGLObject& other)     = delete;
    OpenGLObject& operator=(OpenGLObject&& other) noexcept = delete;


    operator GLuint() const noexcept { return m_id; }
    GLuint operator*() const noexcept { return m_id; }
    GLuint id() const noexcept { return m_id; }

protected:
    GLuint m_id = 0;
};


// the target is only to make thread safe Counter
// and avoid indirection to get opengl object id
/*
------------------------------------------------------------------------------------
Benchmark                                          Time             CPU   Iterations
------------------------------------------------------------------------------------
BM_ConstructRefCounted/iterations:1000000       34.7 ns         31.2 ns      1000000
BM_ConstructSharedPtr/iterations:1000000        40.5 ns         46.9 ns      1000000
BM_GetValueRefCounted/iterations:1000000        34.0 ns         31.2 ns      1000000
BM_GetValueSharedPtr/iterations:1000000         61.6 ns         62.5 ns      1000000
*/

template<typename OpenGLType>
struct CountedGLObject final {
    static_assert(std::derived_from<OpenGLType, OpenGLObject>);

    template<typename... Args>
    requires(!std::disjunction_v<std::is_same<std::remove_cvref_t<Args>, CountedGLObject<OpenGLType>>...>)
    CountedGLObject(Args&&... args)
      : m_ref_counter(new std::atomic_int(1)), m_obj(std::forward<Args>(args)...) {} //-V2511

    ~CountedGLObject() noexcept {
        if (m_ref_counter && m_ref_counter->fetch_sub(1, std::memory_order_acq_rel) == 1) {
            delete m_ref_counter; //-V2511
            m_obj.clean();
        }
    };

    CountedGLObject(const CountedGLObject& other) : m_ref_counter(other.m_ref_counter), m_obj(other.m_obj) {
        if (m_ref_counter && m_ref_counter->fetch_add(1, std::memory_order_relaxed) < 1) {
            SPDLOG_CRITICAL("Memory corruption detected {}", ct::NAME<std::remove_cvref_t<decltype(*this)>>);
            assert(false);
        }
    };

    CountedGLObject(CountedGLObject&& other) noexcept
      : m_ref_counter(std::exchange(other.m_ref_counter, nullptr)), m_obj(std::move(other.m_obj)) {
        if (useCount() < 1) {
            SPDLOG_CRITICAL("Memory corruption detected {}", ct::NAME<std::remove_cvref_t<decltype(*this)>>);
            assert(false);
        }
    }

    CountedGLObject& operator=(const CountedGLObject& other) {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, other);
        return *this;
    };

    CountedGLObject& operator=(CountedGLObject&& other) noexcept {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, std::move(other));
        return *this;
    };


    int useCount() const noexcept { return m_ref_counter ? m_ref_counter->load(std::memory_order_relaxed) : 0; }

    const OpenGLType& get() const noexcept { return m_obj; }
    OpenGLType&       get() noexcept { return m_obj; }

    const OpenGLType* operator->() const noexcept { return &m_obj; }
    OpenGLType*       operator->() noexcept { return &m_obj; }

private:
    std::atomic_int* m_ref_counter;
    OpenGLType       m_obj;
};
