#include "engine/system/save_load.h"
#include "engine/manager/filesystem.h"
#include "engine/manager/key.h"
#include "engine/render/game_render.h" // IWYU pragma: exports
#include <simple-ecs/serializer.h>
#include <render/helpers/helpers.h>


SaveLoadDebugSystem::SaveLoadDebugSystem(World& world) : m_world(world) {};

void SaveLoadDebugSystem::setup(Registry& reg) {
    ZoneScoped;

    m_serializer = &reg.serializer();

    ECS_REG_FUNC(reg, SaveLoadDebugSystem::proceccHotkeys);

    RENDER_ADD_MEMBER_WINDOW(saveWin);
    RENDER_ADD_MEMBER_WINDOW(loadWin);
    RENDER_ADD_MEMBER_WINDOW(removeWin);
    RENDER_ADD_MEMBER_WINDOW(wipeWorldWin);
}

void SaveLoadDebugSystem::saveWin() {
    static std::string file_name;

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("World")) {
            if (ImGui::ScopedBeginMenu("Save")) {
                ImGui::InputText("##filename", &file_name);
                if (ImGui::MenuItem("Save world", nullptr, nullptr)) {
                    save(file_name);
                }

                ImGui::Separator();

                const auto& nodes = FILE_SYSTEM.ls("/save");
                for (const auto& node : nodes) {
                    if (ImGui::MenuItem(node.name.c_str(), nullptr, nullptr)) {
                        save(node.name);
                    }
                }
            }
        }
    }
}


void SaveLoadDebugSystem::loadWin() {
    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("World")) {
            if (ImGui::ScopedBeginMenu("Load")) {
                const auto& nodes = FILE_SYSTEM.ls("/save");
                for (const auto& node : nodes) {
                    if (ImGui::MenuItem(node.name.c_str(), nullptr, nullptr)) {
                        load(node.name);
                    }
                }
            }
        }
    }
}


void SaveLoadDebugSystem::removeWin() {
    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("World")) {
            if (ImGui::ScopedBeginMenu("Remove")) {
                const auto& nodes = FILE_SYSTEM.ls("/save");
                for (const auto& node : nodes) {
                    if (ImGui::MenuItem(node.name.c_str(), nullptr, nullptr)) {
                        remove(node.name);
                    }
                }
            }
        }
    }
}


void SaveLoadDebugSystem::wipeWorldWin() {
    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("World")) {
            ImGui::Separator();
            if (ImGui::MenuItem("Wipe world", nullptr, nullptr)) {
                m_world.destroy(m_world.entities());
            }
        }
    }
}

void SaveLoadDebugSystem::save(const std::string& name) {
    if (name.empty()) {
        spdlog::warn("save name cannot be empty");
        return;
    }

    RENDER_DISPATCH_END_FRAME([this, name] {
        ZoneScopedN("Save level");
        auto  data = m_serializer->save();
        auto& fs   = FILE_SYSTEM;
        fs.mkdir("/save"_t);
        fs.rm("/save/"_t + name);
        fs.write("/save/"_t + name, data);
    });
}

void SaveLoadDebugSystem::load(const std::string& name) {

    RENDER_DISPATCH_END_FRAME([this, name] {
        ZoneScopedN("Load level");
        m_world.destroy(m_world.entities());
        auto&& data = FILE_SYSTEM.read("/save/"_t + name);
        m_serializer->load(std::move(data));
    });
}

void SaveLoadDebugSystem::remove(const std::string& name) {
    FILE_SYSTEM.rm("/save/"_t + name);
}

void SaveLoadDebugSystem::proceccHotkeys(OBSERVER_EMPTY) {
    static bool is_pressed = false;

    if (KEYBOARD_MANAGER.key(GLFW_KEY_F5) && !is_pressed) {
        save("quick");
    }
    if (KEYBOARD_MANAGER.key(GLFW_KEY_F8) && !is_pressed) {
        load("quick");
    }

    is_pressed = KEYBOARD_MANAGER.key(GLFW_KEY_F5) || KEYBOARD_MANAGER.key(GLFW_KEY_F8);
}
