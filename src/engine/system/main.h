#pragma once

#include <simple-ecs/base_system.h>
#include <simple-ecs/world.h>

struct MainEngineSystem final : BaseSystem {
    MainEngineSystem(World& world);
    ~MainEngineSystem() override = default;
    void setup(Registry& reg);

    bool isDone();
    void loadLevel();

private:
    World& m_world;
};