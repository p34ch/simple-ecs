#include "engine/system/camera.h"
#include "engine/components/common.h"
#include "engine/manager/key.h"
#include "engine/manager/mouse.h"
#include "engine/manager/time.h"
#include "engine/render/game_render.h"


void CameraSystem::setup(Registry& reg) {
    ZoneScoped;

    ECS_REG_FUNC(reg, CameraSystem::updateCamera);
    ECS_REG_FUNC(reg, CameraSystem::uploadCameraChanges);

    m_view_and_proj = BUFFER_MANAGER.create<GL_UNIFORM_BUFFER>();
    m_view_and_proj.allocate<GL_DYNAMIC_DRAW, glm::mat4>(2);
    m_view_and_proj.bindBase(0);

    m_view_pos = BUFFER_MANAGER.create<GL_UNIFORM_BUFFER>();
    m_view_pos.allocate<GL_DYNAMIC_DRAW, glm::vec3>(1);
    m_view_pos.bindBase(1);
}


void CameraSystem::updateCamera(OBSERVER(UpdateCameraFilter) observer) {
    ZoneScoped;

    for (auto e : observer) {
        auto& [translation, rotation, scale] = e.get<Transform>();
        auto& [pitch, yaw, view]             = e.get<Camera>();

        const auto& roll  = glm::vec3(0.F, 1.F, 0.F);
        const auto& speed = e.get<Speed>().speed;
        const float camera_speed =
          (speed * TIME_MANAGER.delta())                                               // default
          + (10.F * KEYBOARD_MANAGER.key(GLFW_KEY_LEFT_SHIFT) * TIME_MANAGER.delta()); // extra //-V2564

        auto dir = glm::vec3{rotation.x, 0, rotation.z};

        bool need_update = false;
        if (KEYBOARD_MANAGER.key(GLFW_KEY_W)) {
            need_update = true;
            translation += glm::normalize(dir) * camera_speed;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_S)) {
            need_update = true;
            translation -= glm::normalize(dir) * camera_speed;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_A)) {
            need_update = true;
            translation -= glm::normalize(glm::cross(dir, roll)) * camera_speed;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_D)) {
            need_update = true;
            translation += glm::normalize(glm::cross(dir, roll)) * camera_speed;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_C) /* && position.y > 4*/) {
            need_update = true;
            translation.y -= camera_speed / 2.F;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_SPACE) /*&& position.y < 10*/) {
            need_update = true;
            translation.y += camera_speed / 2.F;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_Q)) {
            need_update = true;
            // yaw -= camera_speed * 10.F;
            translation.y -= camera_speed / 2.F;
        }

        if (KEYBOARD_MANAGER.key(GLFW_KEY_E)) {
            need_update = true;
            // yaw += camera_speed * 10.F;
            translation.y += camera_speed / 2.F;
        }

        if (MOUSE_MANAGER.button(GLFW_MOUSE_BUTTON_RIGHT)) {
            need_update                      = true;
            const auto& [offset_x, offset_y] = MOUSE_MANAGER.offset();

            yaw += offset_x;
            pitch += offset_y;
            pitch = std::clamp(pitch, -89.F, 89.F);
        }

        if (need_update) {
            rotation.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
            rotation.y = sin(glm::radians(pitch));
            rotation.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));

            view = glm::lookAt(translation, translation + rotation, roll);

            e.markUpdated<Camera>();
        }
    }
}


void CameraSystem::uploadCameraChanges(OBSERVER(uploadCameraChangesFilter) observer) {
    ZoneScoped;

    for (auto e : observer) {
        const auto& view      = e.get<Camera>().view;
        const auto& transform = e.get<Transform>();

        const glm::vec3& zoom = transform.scale;
        const glm::mat4& projection =
          glm::perspective(glm::radians(45.0F * zoom[0]),
                           static_cast<float>(RENDER.size().first) / static_cast<float>(RENDER.size().second),
                           zoom[1],
                           zoom[2]);

        RENDER_DISPATCH_END_FRAME([this, projection, view = view, translation = transform.translation] {
            ZoneScopedN("Upload camera");
            m_view_and_proj.set(0, &view);
            m_view_and_proj.set(1, &projection);
            m_view_pos.set(0, &translation);
        });

        e.clearUpdateTag<Camera>();
    }
}