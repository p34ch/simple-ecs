#include "engine/system/main.h"
#include "engine/render/game_render.h"
#include "engine/system/model.h"
#include "engine/system/save_load.h"
#include <imgui.h>
#include <imgui_stdlib.h>


MainEngineSystem::MainEngineSystem(World& world) : m_world(world) {
    ZoneScoped;

    registerComponents(m_world);
};

void MainEngineSystem::setup(Registry& reg) {
    ZoneScoped;

    reg.addSystem<SaveLoadDebugSystem>(m_world);
    reg.addSystem<ModelLoaderSystem>();

    auto* debuger = reg.getSystem<EntityDebugSystem>();

    auto add_debuger_for = [debuger]<typename F>(std::string_view name, F&& cb) {
        return [debuger, name, cb = std::forward<F>(cb)]() {
            if (ImGui::ScopedBeginMainMenuBar()) {
                if (ImGui::ScopedBeginMenu("Debug")) {
                    ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
                }
            }
            std::invoke(std::cref(cb), debuger, RENDER.windowState(name));
        };
    };

    RENDER_ADD_WINDOW(add_debuger_for("All components", &EntityDebugSystem::showRegisteredComponents));
    RENDER_ADD_WINDOW(add_debuger_for("All functions", &EntityDebugSystem::showRegisteredFunctions));
    RENDER_ADD_WINDOW(add_debuger_for("Entities", &EntityDebugSystem::showEntityListUI));

    reg.initNewSystems();
}


bool MainEngineSystem::isDone() {
    return RENDER.isAlive();
}


void MainEngineSystem::loadLevel() {
    auto observer = Observer(m_world);

    auto c = observer.create<CameraType>();
    c.emplaceTagged<Camera>();

    ChairModel chair;
    chair.translation = {8, 0, 0};
    chair.rotation    = {0, 90, 0};
    chair.scale       = {0.1, 0.1, 0.1};
    observer.create(chair);

    CarModel car;
    car.rotation = {-90, 0, 0};
    car.scale    = {10, 10, 10};
    observer.create(car);

    LightType global_light;
    global_light.translation = {-0.7, 50, -2.4};
    global_light.rotation    = {0, -0.5, 0};
    global_light.scale       = {0.1, 0.1, 0.1};

    auto l = observer.create(global_light);
    l.emplace<DirectLight>({.direction = {{0.1, -0.995, 0}}});
}
