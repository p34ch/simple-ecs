#pragma once

#include "engine/components/common.h"
#include "engine/manager/mesh.h" // IWYU pragma: exports
#include "engine/manager/shader.h"
#include "engine/manager/texture.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <utility>


struct ModelGL final : NoCopyNoMove, private SparseSet {
    static constexpr std::array supported_textures = {aiTextureType_DIFFUSE,
                                                      aiTextureType_SPECULAR,
                                                      aiTextureType_AMBIENT,
                                                      aiTextureType_SHININESS,
                                                      aiTextureType_HEIGHT};

    struct Part {
        Mesh                                             mesh;
        std::array<Texture2D, supported_textures.size()> textures;
    };

    ModelGL(std::string name) : m_model_name(std::move(name)) {};
    ~ModelGL() override { spdlog::debug("Unload model {}", m_model_name); }

    const std::vector<Part>&      parts() const noexcept { return m_parts; }
    size_t                        count() const noexcept { return m_positions.size(); }
    const std::vector<glm::mat4>& positions() const noexcept { return m_positions; }
    Shader&                       shader() noexcept { return m_shader; }
    const Shader&                 shader() const noexcept { return m_shader; }
    void                          setShader(const Shader& shader) { m_shader = shader; }
    void                          setFileName(const std::string& name) { m_model_name = name; }
    bool                          isLoaded() const noexcept { return m_is_loaded; }
    void                          setLoaded() noexcept { m_is_loaded = true; }
    bool                          isDirty() const noexcept { return m_is_dirty; }
    void                          setDirty() noexcept { m_is_dirty = true; }

    std::string path() const { return m_model_name.substr(0, m_model_name.find_last_of('/') + 1); }
    std::string fileName() const { return m_model_name.substr(m_model_name.find_last_of('/') + 1); }
    std::string extension() const { return m_model_name.substr(m_model_name.find_last_of('.') + 1); }
    std::string modelName() const { return m_model_name; }
    std::string file() const { return m_model_name; }


    Part& addPart(Part&& part = {}) {
        ZoneScoped;
        m_parts.emplace_back(std::forward<Part>(part));
        return m_parts.back();
    }

    void assignTo(Entity e) {
        ZoneScoped;
        if (emplace(e)) {
            m_positions.emplace_back(1.0F);
        }
    }

    void update(Entity e, const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale) {
        ZoneScoped;
        assert(has(e));
        m_positions[m_sparse[e]] = calcPosition(translation, rotation, scale);
    }

    void update(Entity e, const Transform& transform) {
        ZoneScoped;
        const auto& [translation, rotation, scale] = transform;
        update(e, translation, rotation, scale);
    }

    void erase(Entity e) {
        ZoneScoped;
        if (has(e)) {
            std::swap(m_positions.back(), m_positions[m_sparse[e]]);
            m_positions.pop_back();
            SparseSet::erase(e);
        }
    }

    void sync() {
        ZoneScoped;
        for (auto& p : m_parts) {
            p.mesh.setPositions(m_positions);
        }
        m_is_dirty = false;
    }

    void sync(Entity e) {
        ZoneScoped;
        assert(has(e));
        for (auto& p : m_parts) {
            p.mesh.updatePositions(e, &m_positions[m_sparse[e]]);
        }
        m_is_dirty = false;
    }

    void draw() const {
        ZoneScoped;
        for (const auto& [mesh, _] : m_parts) {
            mesh.draw();
        }
    }

    void draw(int render_mode) const {
        ZoneScoped;
        draw(render_mode, m_shader);
    }

    void draw(int render_mode, Shader shader) const {
        ZoneScoped;
        using namespace std::string_literals;

        if (!shader.get()) {
            shader = m_shader;
        }

        for (const auto& [mesh, textures] : m_parts) {
            for (std::size_t i = 0; i < textures.size(); i++) {
                textures[i]->id() ? textures[i]->bind(i) : TextureGL<GL_TEXTURE_2D>::unbind(i); //-V107
                shader->load("uMaterial."_t->append(aiTextureTypeToString(ModelGL::supported_textures[i])).data(),
                             static_cast<int>(i));
            }

            mesh.draw(render_mode);
        }
    }

private:
    static glm::mat4 calcPosition(const glm::vec3& translation,
                                  const glm::vec3& rotation,
                                  const glm::vec3& scale) noexcept {
        ZoneScoped;
        glm::mat4 coords = glm::translate(glm::mat4(1.0F), translation);

        const auto& angle = (rotation / 180.F) * glm::pi<float>();
        coords *= glm::eulerAngleXYZ(angle.x, angle.y, angle.z);

        coords = glm::scale(coords, scale);
        return coords;
    }

private:
    std::string            m_model_name;
    std::vector<Part>      m_parts;
    std::vector<glm::mat4> m_positions;
    Shader                 m_shader;
    bool                   m_is_loaded : 1 = false;
    bool                   m_is_dirty : 1  = true;
};


struct ModelLoaderSystem final : BaseSystem {
    ModelLoaderSystem();
    void setup(Registry& reg);

    ModelGL*           getPermanent(const std::string&);
    Model              get(const std::string&);
    std::vector<Model> getAll() const {
        std::lock_guard _{m_mutex};
        return m_models;
    }

    void getFiltered(std::vector<Model>&);
    void flush();

private:
    void  showUI();
    Model getImpl(const std::string&);

private:
    using ModelRequestFilter         = Filter<Require<ModelRequest, Transform>, Exclude<ModelComponent>>;
    using ModelTransformUpdateFilter = Filter<Require<ModelComponent, Updated<Transform>>>;
    using ModelUpdateFilter          = Filter<Require<Updated<ModelComponent>>>;

    void resolve(OBSERVER(ModelRequestFilter));
    void update(OBSERVER(ModelTransformUpdateFilter));
    void publish(OBSERVER(ModelUpdateFilter));

private:
    mutable TracyLockable(std::mutex, m_mutex);

    std::vector<Model> m_permanent_models;
    std::vector<Model> m_models;
    std::vector<Model> m_scheduled_to_remove;
};