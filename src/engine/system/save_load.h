#pragma once

struct SaveLoadDebugSystem final : BaseSystem {
    SaveLoadDebugSystem(World& world);
    ~SaveLoadDebugSystem() override = default;
    void setup(Registry& reg);

    void save(const std::string& file_name);
    void load(const std::string& name);
    void remove(const std::string& name);

private:
    void proceccHotkeys(OBSERVER_EMPTY);

private:
    void saveWin();
    void loadWin();
    void removeWin();
    void wipeWorldWin();

private:
    World&      m_world;
    Serializer* m_serializer = nullptr;
};
