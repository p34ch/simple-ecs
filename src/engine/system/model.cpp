#include "engine/system/model.h"
#include "engine/components/common.h"
#include "engine/components/register.h"
#include "engine/manager/filesystem.h"
#include "engine/manager/mesh.h"
#include "engine/manager/texture.h"
#include "engine/render/game_render.h" // IWYU pragma: export
#include <render/render.h>
#include <tasks/tasks.h>


#include <algorithm>
#include <filesystem>

namespace detail::model
{

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 tex_coords;
    glm::vec3 tangent;
};

struct Instance {
    glm::mat4 transform;
};


static auto has_name = [](const std::string& name) {
    return [&name](const Model& v) { return v->modelName() == name; };
};

static auto is_loaded = [](bool is_it) { return [is_it](const Model& v) { return v->isLoaded() == is_it; }; };

static void loadModel(const Model& model) {
    RENDER_KEEP_ALIVE_SCOPE_GUARD;
    tracy::SetThreadName("TASKS Thread");
    ZoneScoped;

    auto               dir  = model->path();
    const std::string& name = model->modelName();

    TracyMessageStr("loading model:" + name);
    spdlog::debug("loading model {}", name);


    Assimp::Importer importer;
    const aiScene*   scene = nullptr;

    if (std::filesystem::exists(name)) {
        ZoneScopedN("Load model from fs");

        scene = importer.ReadFile(name.c_str(),
                                  aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_CalcTangentSpace |
                                    aiProcess_OptimizeMeshes | aiProcess_ValidateDataStructure);
    } else {
        ZoneScopedN("Load model from memory");

        FileSystem::DataOutput data;
        {
            ZoneScopedN("Read raw data from sqlitefs");
            data = FILE_SYSTEM.read(model->file());
            TracyMessageStr("model size:" + std::to_string(data.size()));
        }

        scene = importer.ReadFileFromMemory(data.data(),
                                            data.size(),
                                            aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_CalcTangentSpace |
                                              aiProcess_OptimizeMeshes | aiProcess_ValidateDataStructure,
                                            model->extension().c_str());
    }

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        spdlog::warn("Error. Assimp: '{}' - {}", name, importer.GetErrorString());
        return;
    }


    std::queue<aiNode*> q;
    q.emplace(scene->mRootNode);

    while (!q.empty()) { // BFS
        ZoneScopedN("read model part");
        auto* node = q.front();
        q.pop();

        for (unsigned int i = 0; i < node->mNumChildren; i++) {
            q.emplace(node->mChildren[i]);
        }

        for (unsigned int i = 0; i < node->mNumMeshes; i++) {
            ZoneScopedN("read model mesh part");
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

            std::vector<detail::model::Vertex> part_vertices;
            std::vector<unsigned int>          part_indices;
            std::vector<std::string>           part_textures(ModelGL::supported_textures.size(), "/textures/empty.jpg");
            std::vector<TextureData>           part_textures_data(ModelGL::supported_textures.size());

            // vertices
            part_vertices.resize(mesh->mNumVertices);
            for (unsigned int j = 0; j < mesh->mNumVertices; j++) {
                part_vertices[j].position = glm::make_vec3(&mesh->mVertices[j].x);
                part_vertices[j].normal   = glm::make_vec3(&mesh->mNormals[j].x);
                if (mesh->mTextureCoords[0]) {
                    part_vertices[j].tex_coords = glm::make_vec2(&mesh->mTextureCoords[0][j].x);
                }
                if (mesh->mTangents) {
                    part_vertices[j].tangent = glm::make_vec3(&mesh->mTangents[j].x);
                }
            }

            // faces
            std::span faces{mesh->mFaces, mesh->mNumFaces};
            part_indices.reserve(mesh->mNumFaces * 3ULL);
            for (const auto& face : faces) {
                assert(face.mNumIndices == 3);
                std::span face_indices{face.mIndices, face.mNumIndices};
                part_indices.insert(part_indices.end(), face_indices.begin(), face_indices.end());
            }

            // loading materials
            aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
            for (std::size_t j = 0; j < ModelGL::supported_textures.size(); j++) {
                aiString texture_name;

                if (material->GetTexture(ModelGL::supported_textures[j], 0, &texture_name) == AI_SUCCESS) {
                    const auto* texture = scene->GetEmbeddedTexture(texture_name.C_Str());
                    part_textures[j]    = texture_name.C_Str();

                    if (!texture) {
                        if (!TEXTURE_MANAGER.has(texture_name.C_Str())) {
                            part_textures_data[j] = TEXTURE_MANAGER.loadTextureFromFile(dir + texture_name.C_Str());
                        }
                        continue;
                    }

                    if (texture->mHeight) {
                        TracyMessageStr("loading texture from mesh data");
                        auto*       begin     = reinterpret_cast<unsigned char*>(texture->pcData);
                        std::size_t size      = sizeof(*texture->pcData) * texture->mWidth * texture->mHeight;
                        auto        view      = std::span{begin, size};
                        part_textures_data[j] = {.width  = texture->mHeight,
                                                 .height = texture->mHeight,
                                                 .format = GL_RGBA,
                                                 .data   = {view.begin(), view.end()}};
                    } else { // compressed
                        auto*       begin = reinterpret_cast<char*>(texture->pcData);
                        std::size_t size  = texture->mWidth;
                        auto        view  = std::span{begin, size};

                        auto decompressed     = TEXTURE_MANAGER.loadTextureFromMemory(view);
                        part_textures_data[j] = std::move(decompressed);
                    }
                }
            }

            RENDER_DISPATCH_END_FRAME_SLICED([&model,
                                              part_vertices      = std::move(part_vertices),
                                              part_indices       = std::move(part_indices),
                                              part_textures      = std::move(part_textures),
                                              part_textures_data = std::move(part_textures_data)] {
                ZoneScopedN("load model part");
                auto& pt = model->addPart();

                pt.mesh = MESH_MANAGER.create();

                pt.mesh.setData(part_vertices, part_indices);
                pt.mesh.setAttribPointer(0, &detail::model::Vertex::position);
                pt.mesh.setAttribPointer(1, &detail::model::Vertex::normal);
                pt.mesh.setAttribPointer(2, &detail::model::Vertex::tex_coords);
                pt.mesh.setAttribPointer(3, &detail::model::Vertex::tangent);
                pt.mesh.setAttribPointer(12, &detail::model::Instance::transform, 1);

                // loading materials
                for (std::size_t j = 0; j < ModelGL::supported_textures.size(); j++) {
                    if (TEXTURE_MANAGER.has(part_textures[j])) {
                        pt.textures[j] = TEXTURE_MANAGER.get(part_textures[j]);
                    } else {
                        pt.textures[j] = TEXTURE_MANAGER.create(part_textures[j]);
                        pt.textures[j]->applyTextureData(part_textures_data[j]);
                    }
                }
            });
        }
    }

    RENDER_DISPATCH_END_FRAME_SLICED([model] {
        ZoneScopedN("End load model");
        model->setShader(SHADER_MANAGER.create("default"));
        model->setLoaded();
    });

    spdlog::debug("loaded model {}", name);
}
} // namespace detail::model


// system ----------------------------------------------------

ModelLoaderSystem::ModelLoaderSystem() {
    RENDER_ADD_MEMBER_WINDOW(showUI);
}

void ModelLoaderSystem::setup(Registry& reg) {
    ZoneScoped;

    using namespace std::literals;

    ECS_REG_FUNC(reg, ModelLoaderSystem::resolve);
    ECS_REG_FUNC(reg, ModelLoaderSystem::update);
    ECS_REG_FUNC(reg, ModelLoaderSystem::publish);
}

void ModelLoaderSystem::resolve(OBSERVER(ModelRequestFilter) observer) {
    ZoneScoped;

    for (auto e : observer) {
        const auto& request = e.get<ModelRequest>();
        if (request.name == EMPTY) {
            continue;
        }

        auto model = get(request.name);
        model->assignTo(e);

        e.emplaceTagged<ModelComponent>(model);
        e.markUpdated<Transform>();
    }
}


void ModelLoaderSystem::update(OBSERVER(ModelTransformUpdateFilter) observer) {
    ZoneScoped;

    for (const auto& e : observer) {
        const auto& [model, transform] = e.get();
        model.model->update(e, transform);
        e.markUpdated<ModelComponent>();
    }

    observer.clearUpdateTag<Transform>();
}

void ModelLoaderSystem::publish(OBSERVER(ModelUpdateFilter) observer) {
    ZoneScoped;

    auto models_to_update = TmpBuffer::get<std::vector<ModelGL*>>();
    for (auto e : observer) {
        const auto& model = e.get<ModelComponent>();
        models_to_update->emplace_back(model.model.get());
    }

    if (!models_to_update->empty()) {
        std::sort(models_to_update->begin(), models_to_update->end());
        models_to_update->erase(std::unique(models_to_update->begin(), models_to_update->end()),
                                models_to_update->end());
        for (const auto& model : *models_to_update) {
            model->setDirty();
        }

        observer.clearUpdateTag<ModelComponent>();
    }
}


ModelGL* ModelLoaderSystem::getPermanent(const std::string& name) {
    ZoneScoped;

    std::lock_guard _{m_mutex};

    auto pos = std::ranges::find_if(m_permanent_models, detail::model::has_name(name));
    if (pos != m_permanent_models.end()) {
        return pos->get();
    }

    auto model = getImpl(name);
    auto lower = std::ranges::lower_bound(m_permanent_models, model);
    m_permanent_models.insert(lower, model);

    return model.get();
}

Model ModelLoaderSystem::get(const std::string& name) {
    ZoneScoped;

    std::lock_guard _{m_mutex};
    return getImpl(name);
}

Model ModelLoaderSystem::getImpl(const std::string& name) {
    ZoneScoped;

    auto pos = std::ranges::find_if(m_models, detail::model::has_name(name));
    if (pos != m_models.end()) {
        return *pos;
    }

    spdlog::debug("request to load model {}", name);
    auto model = std::make_shared<ModelGL>(name);
    m_models.emplace_back(model);

    tasks::start<tasks::Priority::IDLE>(&detail::model::loadModel, model);

    return model;
}

void ModelLoaderSystem::getFiltered(std::vector<Model>& completed) {
    ZoneScoped;

    completed.clear();
    std::lock_guard _{m_mutex};
    std::ranges::copy_if(m_models, std::back_inserter(completed), detail::model::is_loaded(true));
}

void ModelLoaderSystem::flush() {
    std::lock_guard _{m_mutex};

    const auto [first, last] = std::ranges::remove_if(m_models, [](const auto& v) { return v.use_count() == 1; });

    for (auto it = first; it != last; ++it) {
        m_scheduled_to_remove.emplace_back(std::move(*it));
        RENDER_DISPATCH_END_FRAME_SLICED([this] {
            std::lock_guard _{m_mutex};
            assert(!m_scheduled_to_remove.empty());
            m_scheduled_to_remove.pop_back();
        });
    }

    PROFILE_ONLY(TracyMessageStr("models were removed: " + std::to_string(std::distance(first, last))));

    m_models.erase(first, last);

    RENDER_DISPATCH_END_FRAME_SLICED([] { TEXTURE_MANAGER.flush(); });
};

void ModelLoaderSystem::showUI() {
    constexpr std::string_view name = "Models";

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Debug")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    constexpr ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY;
    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        if (ImGui::Button("Clear")) {
            flush();
        }

        if (ImGui::ScopedBeginTable("ModelsTable", 2, flags)) {
            ImGui::TableSetupScrollFreeze(0, 1); // Make top row always visible
            ImGui::TableSetupColumn("Name");
            ImGui::TableSetupColumn("Use", ImGuiTableColumnFlags_WidthFixed, 0.0f);
            ImGui::TableHeadersRow();

            std::lock_guard _{m_mutex};
            for (const auto& model : m_models) {
                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                ImGui::Selectable(model->modelName().c_str(), false, ImGuiSelectableFlags_SpanAllColumns);

                ImGui::TableNextColumn();
                ImGui::Text("%ld", model.use_count());
            }
        }
    }
}
