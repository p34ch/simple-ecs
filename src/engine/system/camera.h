#pragma once

#include "engine/components/common.h"
#include "engine/manager/buffer.h" // IWYU pragma: exports
#include <glm/glm.hpp>


struct CameraSystem final : BaseSystem {
    CameraSystem()           = default;
    ~CameraSystem() override = default;
    void setup(Registry& reg);

private:
    using UpdateCameraFilter        = Filter<Require<Camera, Speed, Transform>>;
    using uploadCameraChangesFilter = Filter<Require<Updated<Camera>, Transform>>;

    void updateCamera(OBSERVER(UpdateCameraFilter));
    void uploadCameraChanges(OBSERVER(uploadCameraChangesFilter));

private:
    UniformBuffer m_view_and_proj;
    UniformBuffer m_view_pos;
};