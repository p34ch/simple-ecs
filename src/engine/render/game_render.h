#pragma once

#include "engine/components/common.h"
#include "engine/render/render_utils.h"
#include "engine/render/screen.h"
#include "engine/render/skybox.h"
#include <render/engine/opengl/opengl.h>
#include <render/render.h>
#include <render/widgets/custom_check_box.h>
#include <tracy/TracyOpenGL.hpp>

namespace detail::render
{

struct DirectShadow {
    Shader      shader;
    FrameBuffer frame_buffer;
    Texture2D   depth_map;
    glm::mat4   light_space_matrix;
};

struct LightSources {
    template<typename T>
    struct LightHolder {
        glm::vec3 position = {};
        T         light    = {};
    };

    std::array<LightHolder<PointLight>, MAX_POINT_LIGHT> point;
    std::array<LightHolder<SpotLight>, MAX_SPOT_LIGHT>   spot;
    LightHolder<DirectLight>                             direct;
    std::size_t                                          points_count = 0;
    std::size_t                                          spots_count  = 0;
    bool                                                 has_direct   = false;
};

struct DebugTextures {
    Shader                                                        shader;
    FrameBuffer                                                   frame_buffer;
    Texture2D                                                     depth_map;
    std::array<Texture2D, detail::render::ENUM_TEXTURE_TYPE_SIZE> textures;
};

} // namespace detail::render

struct GameOpenGL : render::OpenGL {
    using Parent = render::OpenGL;

    GameOpenGL();

    void setup(const RenderSettings& settings) override;
    void registerCallbacks() override;

protected:
    std::unordered_map<std::string, bool> m_windows_state;
};

struct GameRender final : Render<GameOpenGL> {
    using Parent = Render<GameOpenGL>;
    friend GameOpenGL;

    GameRender();

    void setup(const RenderSettings& settings) override;
    void initWorld(World* world);

    void                renderFrame() override;
    const RenderScreen& screen() const noexcept;
    bool&               windowState(std::string_view name);

private:
    void beginFrame() override;
    bool renderDebug() override;
    void calcShadows();
    void renderGraphics() override;
    void showWindows() override;
    void endFrame() override;

    void updateDebugTextures();
    void showUI();

private:
    // world stuff
    RenderSkybox m_skybox;
    RenderScreen m_screen;

    World*             m_world        = nullptr;
    Registry*          m_reg          = nullptr;
    ModelLoaderSystem* m_model_system = nullptr;

    int m_render_mode = GL_TRIANGLES;

    std::vector<Model>                   m_models;
    struct detail::render::DirectShadow  m_shadow;
    struct detail::render::LightSources  m_lights;
    struct detail::render::DebugTextures m_debug;
};
