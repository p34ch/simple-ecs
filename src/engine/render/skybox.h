#pragma once

#include "engine/manager/mesh.h"    // IWYU pragma: exports
#include "engine/manager/shader.h"  // IWYU pragma: exports
#include "engine/manager/texture.h" // IWYU pragma: exports


struct RenderSkybox final {
    RenderSkybox() = default;
    void setup();
    void render();

private:
    Mesh        m_cube;
    TextureCube m_texture;
    Shader      m_shader;
};
