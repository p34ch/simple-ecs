#include "engine/render/screen.h"
#include "engine/manager/buffer.h"
#include "engine/manager/key.h"
#include "engine/manager/mesh.h"
#include "engine/manager/mouse.h"
#include "engine/manager/shader.h"
#include "engine/manager/texture.h"
#include "engine/render/game_render.h" // IWYU pragma: exports
#include <simple-ecs/base_system.h>
#include <simple-ecs/registry.h>


namespace detail::screen
{
struct Vertex {
    glm::vec2 pos;
    glm::vec2 tex_coord;
};

static inline std::vector<detail::screen::Vertex> vertices{
  {.pos = {-1.0F, 1.0F}, .tex_coord = {0.0F, 1.0F}},  //
  {.pos = {-1.0F, -1.0F}, .tex_coord = {0.0F, 0.0F}}, //
  {.pos = {1.0F, -1.0F}, .tex_coord = {1.0F, 0.0F}},  //

  {.pos = {-1.0F, 1.0F}, .tex_coord = {0.0F, 1.0F}}, //
  {.pos = {1.0F, -1.0F}, .tex_coord = {1.0F, 0.0F}}, //
  {.pos = {1.0F, 1.0F}, .tex_coord = {1.0F, 1.0F}},  //
};
} // namespace detail::screen

void RenderScreen::setup() {
    ZoneScoped;
    m_render_buffer = BUFFER_MANAGER.create<GL_RENDERBUFFER>();

    m_screen = MESH_MANAGER.create();
    m_screen.setData<GL_ARRAY_BUFFER>(detail::screen::vertices);
    m_screen.setAttribPointer(0, &detail::screen::Vertex::pos);
    m_screen.setAttribPointer(1, &detail::screen::Vertex::tex_coord);
    m_screen.setPositions({glm::translate(glm::mat4(1.0F), {0, 0, 0})});

    m_texture = TEXTURE_MANAGER.create<GL_TEXTURE_2D, ECS_DEBUG_RELEASE_SWITCH(true, false)>("screen texture");
    m_texture->setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR);
    m_texture->setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR);

    m_frame_buffer = BUFFER_MANAGER.create<GL_FRAMEBUFFER>();
    m_frame_buffer.setFrameBufferTexture<GL_COLOR_ATTACHMENT0>(m_texture.get());
    m_frame_buffer.setWData<GL_COLOR_ATTACHMENT0>();

    m_screen_shader = SHADER_MANAGER.create("screen");

    RENDER_ADD_MEMBER_WINDOW(viewport);
}


void RenderScreen::update(int w, int h) {
    RENDER_ONLY_RENDER_THREAD;

    ZoneScoped;
    m_texture->setTexImage<GL_FLOAT>(w, h, GL_RGB32F);
    m_render_buffer.setSize(w, h);
    m_frame_buffer.setFramebufferRenderbuffer<GL_DEPTH_STENCIL_ATTACHMENT>(m_render_buffer);

    if (auto status = m_frame_buffer.status(); status != GL_FRAMEBUFFER_COMPLETE) {
        spdlog::warn("RenderScreen is not complete, status: {}", status);
    }
}


void RenderScreen::activate() {
    RENDER_ONLY_RENDER_THREAD;

    ZoneScoped;
    m_frame_buffer.bind<GL_DRAW_FRAMEBUFFER>();
    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (auto status = m_frame_buffer.status(); status != GL_FRAMEBUFFER_COMPLETE) {
        spdlog::warn("RenderScreen is not complete, status: {}", status);
    }
}


void RenderScreen::render() {
    RENDER_ONLY_RENDER_THREAD;

    ZoneScoped;
    FrameBuffer::unbind();
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT);

    // in dev mode we render to the viewport
    ECS_NOT_FINAL_ONLY(return);

    if (m_screen_shader.get()) {
        m_screen_shader->bind();
        m_texture->bind();
        m_screen.draw();
    }
}


void RenderScreen::viewport() {
    RENDER_ONLY_RENDER_THREAD;

    ImGui::ScopedPushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.f, 0.f));
    ImGui::SetNextWindowViewport(ImGui::GetMainViewport()->ID);

    if (ImGui::ScopedBegin("Viewport", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar)) {
        ImGui::Image(m_texture.get(), ImGui::GetWindowContentRegionMax(), ImVec2(0, 1), ImVec2(1, 0));

        MOUSE_MANAGER.setEnabled(ImGui::IsItemHovered());
        KEYBOARD_MANAGER.setEnabled(ImGui::IsItemHovered());
    }
}
