#include "engine/render/game_render.h"

#include "engine/components/common.h"
#include "engine/manager/key.h"
#include "engine/manager/mouse.h"
#include "engine/manager/shader.h"
#include "engine/manager/texture.h"
#include "engine/manager/time.h"
#include "engine/system/camera.h"
#include "engine/system/model.h"
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <implot.h>


GameOpenGL::GameOpenGL() {
    m_window_size = {1280, 720};
}

void GameOpenGL::setup(const RenderSettings& settings) {
    Parent::setup(settings);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_DEBUG_OUTPUT);
}

void GameOpenGL::registerCallbacks() {
    glfwSetKeyCallback(
      m_window, [](GLFWwindow* /*window*/, int key, int, int action, int) { KEYBOARD_MANAGER.setKey(key, action); });
    glfwSetMouseButtonCallback(
      m_window, [](GLFWwindow* /*window*/, int button, int action, int) { MOUSE_MANAGER.setButton(button, action); });

    glfwSetWindowSizeCallback(m_window, [](GLFWwindow* window, int w, int h) {
        ECS_NOT_FINAL_ONLY(return);

        auto* win          = static_cast<RENDER_TYPE*>(glfwGetWindowUserPointer(window));
        win->m_window_size = {w, h};
        win->m_screen.update(w, h);
        win->updateDebugTextures();
    });
}

GameRender::GameRender() {
    addWindow(std ::bind_front(&GameRender::showUI, this));
}

void GameRender::setup(const RenderSettings& settings) {
    Parent::setup(settings);

    // NOTE: C can't use lambda with capture
    // NOTE: m_windows_state will be alive longer than ImGui context
    static std::unordered_map<std::string, bool>& windows_state = m_windows_state;
    static ImGuiSettingsHandler                   ini_handler;

    ini_handler.TypeName = "WindowsState";
    ini_handler.TypeHash = ImHashStr("WindowsState");

    ini_handler.ReadOpenFn = [](ImGuiContext* /*ctx*/, ImGuiSettingsHandler* /*handler*/, const char* /*name*/) {
        return (void*)"States"; // NOLINT
    };

    ini_handler.ReadLineFn =
      [](ImGuiContext* /*ctx*/, ImGuiSettingsHandler* /*handler*/, void* /*entry*/, const char* line) {
          std::string_view str{line};
          auto             pos = str.find('=');
          if (pos == std::string_view::npos) {
              return;
          }
          std::string_view name  = str.substr(0, pos);
          std::string_view value = str.substr(pos + 1);
          auto [_, emplaced]     = windows_state.try_emplace(std::string(name), std::stoi(std::string(value)) != 0);
          assert(emplaced);
      };

    ini_handler.WriteAllFn = [](ImGuiContext* /*ctx*/, ImGuiSettingsHandler* /*handler*/, ImGuiTextBuffer* out_buf) {
        out_buf->appendf("[%s][%s]\n", "WindowsState", "States");
        for (const auto& [name, state] : windows_state) {
            out_buf->appendf("%s=%d\n", name.data(), state); // NOLINT
        }
    };

    ImGui::AddSettingsHandler(&ini_handler);
}


void GameRender::initWorld(World* world) {
    RENDER_ONLY_RENDER_THREAD

    m_world = world;
    m_reg   = m_world->getRegistry();

    m_reg->addSystem<CameraSystem>();
    m_reg->initNewSystems();

    m_skybox.setup();
    m_screen.setup();

    m_model_system = m_reg->getSystem<ModelLoaderSystem>();
    assert(m_model_system);

    // debug
    updateDebugTextures();

    // shadow
    m_shadow.shader = SHADER_MANAGER.create("shadow");

    m_shadow.depth_map =
      TEXTURE_MANAGER.create<GL_TEXTURE_2D, ECS_DEBUG_RELEASE_SWITCH(true, false)>("shadow depth map");
    m_shadow.depth_map->setTexImage<GL_FLOAT>(SHADOW_WIDTH, SHADOW_HEIGHT, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT);
    m_shadow.depth_map->setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR);
    m_shadow.depth_map->setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR);
    m_shadow.depth_map->setTexParam<GL_TEXTURE_WRAP_S>(GL_CLAMP_TO_BORDER);
    m_shadow.depth_map->setTexParam<GL_TEXTURE_WRAP_T>(GL_CLAMP_TO_BORDER);
    m_shadow.depth_map->setTexParam<GL_TEXTURE_BORDER_COLOR>(glm::vec4{0.0F, 0.0F, 0.0F, 1.0F});

    m_shadow.frame_buffer = BUFFER_MANAGER.create<GL_FRAMEBUFFER>();
    m_shadow.frame_buffer.setFrameBufferTexture<GL_DEPTH_ATTACHMENT>(m_shadow.depth_map.get());
    m_shadow.frame_buffer.setWData<GL_NONE>();

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        spdlog::warn("FB error, status: {:x}\n", status);
    }

    m_screen.update(m_window_size.first, m_window_size.second);

    RENDER_ADD_WINDOW([&mode = m_render_mode]() {
        if (ImGui::BeginMainMenuBar()) {
            if (ImGui::BeginMenu("Render")) {
                if (ImGui::BeginMenu("Mode")) {
                    ImGui::Combo("Combo",
                                 &mode,
                                 "GL_POINTS\0"
                                 "GL_LINES\0"
                                 "GL_LINE_LOOP\0"
                                 "GL_LINE_STRIP\0"
                                 "GL_TRIANGLES\0"
                                 "GL_TRIANGLE_STRIP\0"
                                 "GL_TRIANGLE_FAN\0"
                                 "GL_QUADS\0\0");
                    ImGui::EndMenu();
                }
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }
    });
}

const RenderScreen& GameRender::screen() const noexcept {
    return m_screen;
}

bool& GameRender::windowState(std::string_view name) {
    return m_windows_state[name.data()];
}

void GameRender::renderFrame() {
    Parent::renderFrame();
}

void GameRender::beginFrame() {
    {                // mouse
        double x, y; // NOLINT
        glfwGetCursorPos(m_window, &x, &y);
        MOUSE_MANAGER.setPosition(static_cast<float>(x), static_cast<float>(y));
    }

    {
        ZoneScopedN("sync with ecs");
        m_reg->waitFrame();
        m_reg->prepare();
    }

    { // lights
        {
            m_lights.has_direct             = false;
            using PrepareLightsRenderFilter = Filter<Require<DirectLight, Transform>>;
            OBSERVER(PrepareLightsRenderFilter) observer(*m_world);
            for (const auto& e : observer) {
                const auto& [direct_light, transform] = e.get();
                m_lights.direct                       = {.position = transform.translation, .light = direct_light};
                m_lights.has_direct                   = true;
            }
        }

        auto get_light = [world = m_world]<typename Light, typename Container>(Container& container) {
            std::size_t index               = 0;
            using PrepareLightsRenderFilter = Filter<Require<Light, Transform>>;
            OBSERVER(PrepareLightsRenderFilter) observer(*world);
            for (const auto& e : observer) {
                const auto& [light, transform] = e.get();
                container[index++]             = {transform.translation, light};
            }
            return index;
        };

        m_lights.points_count = get_light.operator()<PointLight>(m_lights.point);
        m_lights.spots_count  = get_light.operator()<SpotLight>(m_lights.spot);
    }

    { // models
        m_model_system->getFiltered(m_models);
        for (const auto& model : m_models) {
            if (model->isDirty()) {
                model->sync();
            }
        }
    }
    {
        ZoneScopedN("sync with ecs");
        m_reg->frameSynchronized();
    }
}

bool GameRender::renderDebug() {
#if _DEBUG
    glViewport(0, 0, m_window_size.first, m_window_size.second);
    glEnable(GL_DEPTH_TEST);

    m_debug.shader->bind();
    m_debug.frame_buffer.bind<GL_DRAW_FRAMEBUFFER>();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (const auto& model : m_models) {
        model->draw(m_render_mode, m_debug.shader);
    }

    // recompile shaders
    std::set<ShaderGL*> shaders;
    for (const auto& model : m_models) {
        auto shader = model->shader().get();
        if (shader) {
            shaders.emplace(&shader);
        }
    }

    for (auto* shader : shaders) {
        if (shader->isLiveUpdate()) {
            shader->reload();
        }
    }
#endif
    return true;
}

void GameRender::calcShadows() {
    ZoneScoped;

    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glEnable(GL_DEPTH_TEST);

    m_shadow.shader->bind();
    m_shadow.frame_buffer.bind<GL_DRAW_FRAMEBUFFER>();

    // direct light
    if (m_lights.has_direct) {
        glm::mat4 light_projection = glm::ortho(-100.0F, 100.0F, -100.0F, 100.0F, 10.F, 600.F);
        glm::mat4 light_view       = glm::lookAt(m_lights.direct.position,
                                           m_lights.direct.position + m_lights.direct.light.direction.direction,
                                           glm::vec3(0.0, 1.0, 0.0));

        m_shadow.light_space_matrix = light_projection * light_view;
    }

    m_shadow.shader->load("uLightSpaceMatrix", m_shadow.light_space_matrix);


    glClear(GL_DEPTH_BUFFER_BIT);
    for (const auto& model : m_models) {
        ZoneScopedN("draw model");
        model->draw();
    }
}

void GameRender::renderGraphics() {
    calcShadows();

    glViewport(0, 0, m_window_size.first, m_window_size.second);

    m_screen.activate();
    m_skybox.render();

    std::ranges::sort(m_models, std::less<>{}, [](const Model& ptr) {
        assert(ptr->shader().get());
        return ptr->shader()->id();
    });

    GLuint last_shader_id = 0;

    for (const auto& model : m_models) {
        if (const auto& shader = model->shader(); shader.get() && last_shader_id != shader->id()) {
            last_shader_id = shader->id();
            shader->bind();
            shader->load("uDirectLightEnabled"_t, m_lights.has_direct);
            shader->load("uPointLightsCount"_t, m_lights.points_count);
            shader->load("uSpotLightsCount"_t, m_lights.spots_count);

            if (m_lights.has_direct) {
                shader->load("uDirectLight"_t + ".BaseLight.Ambient", m_lights.direct.light.base.ambient);
                shader->load("uDirectLight"_t + ".BaseLight.Diffuse", m_lights.direct.light.base.diffuse);
                shader->load("uDirectLight"_t + ".BaseLight.Specular", m_lights.direct.light.base.specular);
                shader->load("uDirectLight"_t + ".Power", m_lights.direct.light.power);
                shader->load("uDirectLight"_t + ".Direction", m_lights.direct.light.direction.direction);
            }

            for (std::size_t i = 0; i < m_lights.points_count; i++) {
                const auto& num   = std::to_string(i);
                auto&       point = m_lights.point[i];
                shader->load("uPointLight["_t + num + "].BaseLight.Ambient", point.light.base.ambient);
                shader->load("uPointLight["_t + num + "].BaseLight.Diffuse", point.light.base.diffuse);
                shader->load("uPointLight["_t + num + "].BaseLight.Specular", point.light.base.specular);
                shader->load("uPointLight["_t + num + "].Position", point.position);
                shader->load("uPointLight["_t + num + "].Constant", point.light.constant);
                shader->load("uPointLight["_t + num + "].Linear", point.light.linear);
                shader->load("uPointLight["_t + num + "].Quadratic", point.light.quadratic);
            }

            for (std::size_t i = 0; i < m_lights.spots_count; i++) {
                const auto& num  = std::to_string(i);
                auto&       spot = m_lights.spot[i];
                shader->load("uSpotLight["_t + num + "].PointLight.BaseLight.Ambient", spot.light.point.base.ambient);
                shader->load("uSpotLight["_t + num + "].PointLight.BaseLight.Diffuse", spot.light.point.base.diffuse);
                shader->load("uSpotLight["_t + num + "].PointLight.BaseLight.Specular", spot.light.point.base.specular);
                shader->load("uSpotLight["_t + num + "].PointLight.Position", spot.position);
                shader->load("uSpotLight["_t + num + "].PointLight.Constant", spot.light.point.constant);
                shader->load("uSpotLight["_t + num + "].PointLight.Linear", spot.light.point.linear);
                shader->load("uSpotLight["_t + num + "].PointLight.Quadratic", spot.light.point.quadratic);
                shader->load("uSpotLight["_t + num + "].Direction", spot.light.direction.direction);
                shader->load("uSpotLight["_t + num + "].CutOff", spot.light.cut_off);
                shader->load("uSpotLight["_t + num + "].OuterCutOff", spot.light.outer_cut_off);
            }

            m_shadow.depth_map->bind(10);
            shader->load("uShadowMap"_t, 10);
            shader->load("uLightSpaceMatrix"_t, m_shadow.light_space_matrix);
        }
        model->draw(m_render_mode);
    }

    m_screen.render();

    m_models.clear();
}

void GameRender::showWindows() {
    ZoneScopedN("sync with ecs");
    ECS_NOT_FINAL_ONLY(m_reg->waitFrame());
    ECS_NOT_FINAL_ONLY(Parent::showWindows());
}

void GameRender::endFrame() {
    Parent::endFrame();

    TIME_MANAGER.update();
}

void GameRender::updateDebugTextures() {
    ECS_RELEASE_ONLY(return);
    ZoneScoped;

    m_debug.shader       = SHADER_MANAGER.create("debug");
    m_debug.frame_buffer = BUFFER_MANAGER.create<GL_FRAMEBUFFER>();

    // we use GL_COLOR_ATTACHMENT0 as template param with offset and it requires
    // to be in compile time for loop
    [&]<size_t... Is>(std::index_sequence<Is...>) {
        using detail::render::name;
        using detail::render::TEXTURE_TYPE;

        ((m_debug.textures[Is] = //-V807
          TEXTURE_MANAGER.template create<GL_TEXTURE_2D, ECS_DEBUG_RELEASE_SWITCH(true, false)>(
            std::string(name<static_cast<TEXTURE_TYPE>(Is)>()))),
         ...);

        (m_debug.textures[Is]->template setTexImage<GL_FLOAT>( //-V807
           m_window_size.first,
           m_window_size.second,
           GL_RGB32F),
         ...);
        (m_debug.textures[Is]->template setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR), ...); //-V807
        (m_debug.textures[Is]->template setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR), ...); //-V807

        (m_debug.frame_buffer.template setFrameBufferTexture<GL_COLOR_ATTACHMENT0 + Is>(m_debug.textures[Is].get()),
         ...);
    }(std::make_index_sequence<detail::render::ENUM_TEXTURE_TYPE_SIZE>{});

    m_debug.frame_buffer.setWData<GL_COLOR_ATTACHMENT0>(detail::render::ENUM_TEXTURE_TYPE_SIZE);

    m_debug.depth_map = TEXTURE_MANAGER.create<GL_TEXTURE_2D, ECS_DEBUG_RELEASE_SWITCH(true, false)>("debug depth map");
    m_debug.depth_map->setTexImage<GL_FLOAT>(
      m_window_size.first, m_window_size.second, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT);
    m_debug.depth_map->setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR);
    m_debug.depth_map->setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR);
    m_debug.depth_map->setTexParam<GL_TEXTURE_WRAP_S>(GL_CLAMP);
    m_debug.depth_map->setTexParam<GL_TEXTURE_WRAP_T>(GL_CLAMP);
    m_debug.frame_buffer.setFrameBufferTexture<GL_DEPTH_ATTACHMENT>(m_debug.depth_map.get());

    if (auto status = m_debug.frame_buffer.status(); status != GL_FRAMEBUFFER_COMPLETE) {
        spdlog::warn("Render buffers  is not complete, status: {}", status);
    }
}

void GameRender::showUI() {
    constexpr std::string_view name = "Settings";
    static std::array<int, 2>  size = {m_window_size.first, m_window_size.second};

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Render")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        ImGuiIO& io = ImGui::GetIO();
        ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0F / io.Framerate, io.Framerate);

        ImGui::SliderInt2("Resolution", size.data(), 100, 4096);
        if (ImGui::Button("Apply")) {
            m_window_size = {size[0], size[1]};
            m_screen.update(m_window_size.first, m_window_size.second);
            updateDebugTextures();
        }
    }
}
