#pragma once

#include "engine/manager/buffer.h"  // IWYU pragma: exports
#include "engine/manager/mesh.h"    // IWYU pragma: exports
#include "engine/manager/shader.h"  // IWYU pragma: exports
#include "engine/manager/texture.h" // IWYU pragma: exports


struct RenderScreen final {
    RenderScreen() = default;
    void setup();

    void update(int w, int h);
    void activate();
    void render();
    void flush();

private:
    void viewport();

private:
    Mesh         m_screen;
    FrameBuffer  m_frame_buffer;
    RenderBuffer m_render_buffer;
    Texture2D    m_texture;
    Shader       m_screen_shader;
};