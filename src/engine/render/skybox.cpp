#include "engine/render/skybox.h"
#include "engine/manager/mesh.h"
#include "engine/manager/shader.h"
#include "engine/manager/texture.h"


namespace detail::skybox
{

struct Vertex {
    glm::vec3 pos;
};


static inline std::vector<detail::skybox::Vertex> vertices = {
  // positions
  {{-1.0F, 1.0F, -1.0F}},  //
  {{-1.0F, -1.0F, -1.0F}}, //
  {{1.0F, -1.0F, -1.0F}},  //
  {{1.0F, -1.0F, -1.0F}},  //
  {{1.0F, 1.0F, -1.0F}},   //
  {{-1.0F, 1.0F, -1.0F}},  //

  {{-1.0F, -1.0F, 1.0F}},  //
  {{-1.0F, -1.0F, -1.0F}}, //
  {{-1.0F, 1.0F, -1.0F}},  //
  {{-1.0F, 1.0F, -1.0F}},  //
  {{-1.0F, 1.0F, 1.0F}},   //
  {{-1.0F, -1.0F, 1.0F}},  //

  {{1.0F, -1.0F, -1.0F}}, //
  {{1.0F, -1.0F, 1.0F}},  //
  {{1.0F, 1.0F, 1.0F}},   //
  {{1.0F, 1.0F, 1.0F}},   //
  {{1.0F, 1.0F, -1.0F}},  //
  {{1.0F, -1.0F, -1.0F}}, //

  {{-1.0F, -1.0F, 1.0F}}, //
  {{-1.0F, 1.0F, 1.0F}},  //
  {{1.0F, 1.0F, 1.0F}},   //
  {{1.0F, 1.0F, 1.0F}},   //
  {{1.0F, -1.0F, 1.0F}},  //
  {{-1.0F, -1.0F, 1.0F}}, //

  {{-1.0F, 1.0F, -1.0F}}, //
  {{1.0F, 1.0F, -1.0F}},  //
  {{1.0F, 1.0F, 1.0F}},   //
  {{1.0F, 1.0F, 1.0F}},   //
  {{-1.0F, 1.0F, 1.0F}},  //
  {{-1.0F, 1.0F, -1.0F}}, //

  {{-1.0F, -1.0F, -1.0F}}, //
  {{-1.0F, -1.0F, 1.0F}},  //
  {{1.0F, -1.0F, -1.0F}},  //
  {{1.0F, -1.0F, -1.0F}},  //
  {{-1.0F, -1.0F, 1.0F}},  //
  {{1.0F, -1.0F, 1.0F}}    //
};

} // namespace detail::skybox

void RenderSkybox::setup() {
    ZoneScoped;
    auto sky_name = "/textures/cubemap/sky"_t;
    m_texture     = TEXTURE_MANAGER.create<GL_TEXTURE_CUBE_MAP>(sky_name);

    constexpr std::array<std::string_view, 6> faces{
      "right.png", "left.png", "top.png", "bottom.png", "front.png", "back.png"};

    std::array<TextureData, 6> data;
    for (unsigned int i = 0; i < faces.size(); i++) {
        const auto& file = *sky_name + "/" + faces[i].data();
        spdlog::debug("Load texture {}", file);
        data[i] = TEXTURE_MANAGER.loadTextureFromFile(file);

        if (data[i].data.empty()) {
            spdlog::warn("Cubemap texture failed to load at path: {}", faces[i]);
        }
    }
    m_texture->applyTextureData(data);

    m_cube = MESH_MANAGER.create();
    m_cube.setData<GL_ARRAY_BUFFER>(detail::skybox::vertices);
    m_cube.setAttribPointer(0, &detail::skybox::Vertex::pos);
    m_cube.setPositions({glm::translate(glm::mat4(1.0F), {0, 0, 0})});

    m_shader = SHADER_MANAGER.create("cubemap");
}

void RenderSkybox::render() {
    ZoneScoped;
    glDepthFunc(GL_LEQUAL);

    if (m_shader.get()) {
        m_shader->bind();
        m_texture->bind();
        m_cube.draw();
    }

    glDepthFunc(GL_LESS); // set depth function back to default
}
