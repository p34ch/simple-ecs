#pragma once

constexpr std::size_t  MAX_POINT_LIGHT = 32;
constexpr std::size_t  MAX_SPOT_LIGHT  = 32;
constexpr unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

namespace detail::render
{
CT_MAKE_ENUM(TEXTURE_TYPE, POSITION, NORMAL, TEXCOORD, ALBEDO, SPECULAR, AMBIENT, SHININESS, BUMP);
} // namespace detail::render
