#include "engine/components/common.h"
#include "engine/system/model.h"
#include <simple-ecs/world.h>

#include <algorithm>


void registerComponents(World& world) {
    ECS_DEBUG_ONLY(ComponentRegistrant<DEBUG_CMP>(world).createStorage().addCreateFunc());

    ComponentRegistrant<Skybox, LightSource>(world).createStorage().addDebuger().addCreateFunc().addSerialize();

    ComponentRegistrant<ModelRequest>(world)
      .createStorage()
      .addDebuger()
      .addCreateFunc()
      .setSaveFunc([](const ModelRequest& comp) {
          serializer::Output temp;
          auto&&             size = serializer::serialize(comp.name.size());
          std::ranges::copy(size, std::back_inserter(temp));
          std::ranges::copy(comp.name, std::back_inserter(temp));
          return temp;
      })
      .setLoadFunc([](serializer::Input& data) -> ModelRequest {
          auto        size = serializer::deserialize<std::string::size_type>(data);
          std::string temp(size, 0);
          std::memcpy(temp.data(), data, size);
          data += size;
          return {std::move(temp)};
      });

    ComponentRegistrant<Camera, Transform, Color, PointLight, DirectLight, SpotLight, Direction, Speed>(world)
      .createStorage()
      .addDebuger()
      .addCreateFunc()
      .addSerialize();


    ComponentRegistrant<ModelComponent>(world).createStorage().addDebuger().addDestroyCallback(
      [](Entity e, ModelComponent& c) {
          if (c.model) {
              c.model->erase(e);
              c.model->setDirty();
          }
      });
}