#pragma once

#include "simple-ecs/utils.h"

#define SHADER(X) detail::ShaderMap::map.at(X)
#define EMPTY "empty"

struct ShaderFiles {
    std::string vertex;
    std::string fragment;
};

namespace detail
{

struct ShaderMap {
    const static inline std::unordered_map<std::string, ShaderFiles> map{
      {"default", {"default.vert", "default.frag"}},

      {"debug",
       ECS_DEBUG_RELEASE_SWITCH(ECS_CASE({"default.vert", "debug.frag"}), ECS_CASE({"empty.vert", "empty.frag"}))},

      {"empty", {"empty.vert", "empty.frag"}},
      {"cubemap", {"cubemap.vert", "cubemap.frag"}},
      {"screen", {"screen.vert", "screen.frag"}},
      {"shadow", {"shadow.vert", "shadow.frag"}},

      {"diffuse", {"default.vert", "diffuse.frag"}},
      {"ambient", {"default.vert", "ambient.frag"}},
      {"bump", {"default.vert", "bump.frag"}},
      {"height", {"default.vert", "height.frag"}},
      {"normals", {"default.vert", "normals.frag"}},
      {"tex_coord", {"default.vert", "tex_coord.frag"}},
      {"shiness", {"default.vert", "shiness.frag"}},
      {"specular", {"default.vert", "specular.frag"}},
      {"positions", {"default.vert", "positions.frag"}},
      {"black", {"default.vert", "black.frag"}},
      {"white", {"default.vert", "white.frag"}},
    };
};


} // namespace detail


void registerComponents(World& m_world);
