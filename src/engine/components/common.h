#pragma once

#include "engine/components/register.h"

// tags

struct Skybox {};
struct LightSource {};

// comps
struct Camera {
    float     pitch = 0.F;
    float     yaw   = 0.F;
    glm::mat4 view  = {{0, 0, -1, 0}, {0, 1, 0, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}};
};

struct Transform {
    glm::vec3 translation = {0.0F, 0.0F, 0.0F};
    glm::vec3 rotation    = {0.0F, 0.0F, 0.0F};
    glm::vec3 scale       = {1.0F, 1.0F, 1.0F};
};

struct Direction {
    glm::vec3 direction = {1.0F, 0.0F, 0.0F};
};

struct Color {
    glm::vec3 color = {1.0F, 1.0F, 1.0F};
};

struct Light {
    glm::vec3 ambient  = {1.0F, 1.0F, 1.0F};
    glm::vec3 diffuse  = {1.0F, 1.0F, 1.0F};
    glm::vec3 specular = {1.0F, 1.0F, 1.0F};
};

struct PointLight {
    Light base      = {};
    float constant  = 1.F;
    float linear    = 0.09F;
    float quadratic = 0.032F;
};

struct DirectLight {
    Light     base      = {};
    Direction direction = {};
    float     power     = 1.F;
};

struct SpotLight {
    PointLight point         = {};
    Direction  direction     = {};
    float      cut_off       = 0.85F;
    float      outer_cut_off = 0.82F;
};


struct Speed {
    float speed = 4.5F;
};

struct ModelRequest {
    std::string name = EMPTY;
};


struct ModelComponent {
    Model model;
};


// Archetypes
using CameraArchetype = Archetype<Speed, Transform, Camera>;
struct CameraType : CameraArchetype {
    CameraType() : CameraArchetype(Speed(10), Transform{.scale{1, 10, 1000}}) {}
};

///

using ModelArchetype = Archetype<ModelRequest, Name, Transform>;
struct ChairModel : ModelArchetype {
    ChairModel() : Archetype(ModelRequest("/models/chair/chair.usdz"), Name("chair")) {};
};

struct CarModel : ModelArchetype {
    CarModel() : Archetype(ModelRequest("/models/car/car.usdz"), Name("car")) {};
};

struct BagModel : ModelArchetype {
    BagModel() : Archetype(ModelRequest("/models/bag/bag.usdz"), Name("bag")) {};
};

///

using LightArchetype = Archetype<ModelRequest, Name, Transform, LightSource>;

struct LightType : ModelArchetype {
    LightType() : Archetype(ModelRequest(""), Name("Direct light")) {};
};


// DEBUG

template<>
void debug(Camera& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(Transform& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(Direction& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(Color& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(Light& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(PointLight& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(DirectLight& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(SpotLight& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(Speed& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(ModelRequest& component, Entity entity, bool& toMarkUpdated);

template<>
void debug(ModelComponent& component, Entity entity, bool& toMarkUpdated);


struct DEBUG_CMP {
    int value = 0;
    DEBUG_CMP() { std::cout << "***def constr: " << value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl; };

    DEBUG_CMP(int v) : value(v) {
        std::cout << "***constr: " << v << " l:" << __LINE__ << " f:" << __FILE__ << std::endl;
    }

    DEBUG_CMP(const DEBUG_CMP& v) : value(v.value) {
        std::cout << "***copy: " << v.value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl;
    }

    DEBUG_CMP(DEBUG_CMP&& v) noexcept : value(v.value) {
        std::cout << "***move: " << v.value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl;
    }

    DEBUG_CMP& operator=(const DEBUG_CMP& v) {
        if (this == &v) {
            return *this;
        }

        value = v.value;
        std::cout << "***copy =: " << v.value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl;
        return *this;
    }

    DEBUG_CMP& operator=(DEBUG_CMP&& v) noexcept {
        value = v.value;
        std::cout << "***move =: " << v.value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl;
        return *this;
    }

    ~DEBUG_CMP() { std::cout << "***destr: " << value << " l:" << __LINE__ << " f:" << __FILE__ << std::endl; }
};
