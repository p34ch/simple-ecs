#include "engine/components/common.h"
#include "engine/system/model.h"
#include <imgui_stdlib.h>

template<>
void debug(Camera& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::DragFloat("Pitch", &component.pitch, 0.1F);
    ImGui::DragFloat("Yaw", &component.yaw, 0.1F, -89.F, 89.F);
}

template<>
void debug(Transform& component, Entity /*entity*/, bool& toMarkUpdated) {
    ImGui::DragFloat3("Translation", reinterpret_cast<float*>(&component.translation), 0.1F);
    ImGui::DragFloat3("Rotation", reinterpret_cast<float*>(&component.rotation), 0.1F, -360.F, 360.F);
    ImGui::DragFloat3("Scale", reinterpret_cast<float*>(&component.scale), 0.001F, 0.001F, 1000.F);
    toMarkUpdated = true;
}

template<>
void debug(Color& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::ColorEdit3("Color", reinterpret_cast<float*>(&component.color));
}

template<>
void debug(Direction& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::DragFloat3("Direction", reinterpret_cast<float*>(&component.direction), 0.01F, -1.F, 1.F);
    component.direction = glm::normalize(component.direction);
}

template<>
void debug(Light& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::ColorEdit3("Ambient", reinterpret_cast<float*>(&component.ambient));
    ImGui::ColorEdit3("Diffuse", reinterpret_cast<float*>(&component.diffuse));
    ImGui::ColorEdit3("Specular", reinterpret_cast<float*>(&component.specular));
};

template<>
void debug(PointLight& component, Entity entity, bool& toMarkUpdated) {
    debug(component.base, entity, toMarkUpdated);

    ImGui::SliderFloat("Constant", &component.constant, 0.F, 1.F, "%.3f");
    ImGui::SliderFloat("Linear", &component.linear, 0.F, 1.F, "%.4f");
    ImGui::SliderFloat("Quadratic", &component.quadratic, 0.F, 1.F, "%.5f");
}

template<>
void debug(DirectLight& component, Entity entity, bool& toMarkUpdated) {
    debug(component.base, entity, toMarkUpdated);
    debug(component.direction, entity, toMarkUpdated);

    ImGui::SliderFloat("Power", &component.power, 0.F, 1.F, "%.3f");
}

template<>
void debug(SpotLight& component, Entity entity, bool& toMarkUpdated) {
    debug(component.point, entity, toMarkUpdated);
    debug(component.direction, entity, toMarkUpdated);

    ImGui::SliderAngle("Cut off", &component.cut_off, component.outer_cut_off * 180.F / 3.14F, 180.F, "%.3f");
    ImGui::SliderAngle("Outer cut off", &component.outer_cut_off, 0.F, component.cut_off * 180.F / 3.14F, "%.3f");
}


template<>
void debug(Speed& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::DragFloat("Speed", &component.speed, .1F, 0.F, 25.F);
}

template<>
void debug(ModelRequest& component, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    ImGui::InputText("Path", &component.name);
}

template<>
void debug(ModelComponent& c, Entity /*entity*/, bool& /*toMarkUpdated*/) {
    if (!c.model || !c.model->shader().get()) {
        ImGui::TextUnformatted("Model is not set");
        return;
    }
    Shader& shader = c.model->shader();
    ImGui::SeparatorText("Model");
    ImGui::Text("%s", c.model->modelName().data());
    ImGui::SeparatorText("Shader");
    ImGui::Text("ID: %d", shader->id());

    ImGui::SameLine();
    if (ImGui::SmallButton("reload")) {
        shader->reload();
    }

    ImGui::SameLine();
    if (ImGui::SmallButton("create")) {
        c.model->setShader(SHADER_MANAGER.create(EMPTY, true));
    }

    auto keys = std::views::keys(detail::ShaderMap::map);

    // imgui combo box requires null terminated strings
    // and one more null terminator for the list
    std::vector<char> names;
    for (const auto& name : keys) {
        names.insert(names.end(), name.begin(), name.end());
        names.emplace_back(0);
    }
    names.emplace_back(0);

    int cur = std::distance(keys.begin(), std::ranges::find(keys, shader->name()));
    int old = cur;
    ImGui::Combo("Shader", &cur, names.data());

    auto pos = std::views::keys(detail::ShaderMap::map).begin();
    std::ranges::advance(pos, cur);
    if (old != cur) {
        shader->setName(*pos);
    }

    ECS_DEBUG_ONLY(bool b = shader->isLiveUpdate());
    ECS_DEBUG_ONLY(ImGui::Checkbox("Live Update", &b));
    ECS_DEBUG_ONLY(shader->setLiveUpdate(b));
}
