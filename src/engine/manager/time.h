#pragma once

struct TimeManager final {
    TimeManager();
    float delta() const { return static_cast<float>(m_delta_time); }
    float multiplier() const { return m_multiplier; }
    void  setMultiplier(float val) { m_multiplier = std::clamp(val, 0.25F, 5.0F); }

    void update();
    void showUI();

private:
    size_t m_frame                 = 0;
    double m_last_frame_time_point = 0.0F; // Time of last frame
    double m_delta_time            = 0.0F; // Time between current frame and last frame
    float  m_multiplier            = 1.0;
};

#define TIME_MANAGER Singleton<TimeManager>::instance()
