#pragma once

#include "engine/manager/buffer.h"
#include "engine/manager/filesystem.h"
#include "engine/manager/key.h"
#include "engine/manager/mesh.h"
#include "engine/manager/mouse.h"
#include "engine/manager/shader.h"
#include "engine/manager/texture.h"
#include "engine/manager/time.h"
#include "engine/render/game_render.h"
#include <render/logger/logger.h>


// we want to control time to live for all singletons and alloc/dealloc order
// pybind11 does not provide an explicit mechanism to invoke cleanup code at module destruction time.
struct SingletonsHolder {
    Singleton<render::Logger<"Game">> logger;

    Singleton<RENDER_TYPE> render;

    Singleton<ShaderBuilderManager>  shader_manager;
    Singleton<BufferBuilderManager>  buffer_manager;
    Singleton<MeshBuilderManager>    mesh_manager;
    Singleton<TextureBuilderManager> texture_manager;
    Singleton<KeyboardManager>       keyboard_manager;
    Singleton<MouseManager>          mouse_manager;
    Singleton<TimeManager>           time_manager;
    Singleton<FileSystem>            file_system;
};