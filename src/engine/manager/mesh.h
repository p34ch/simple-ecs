#pragma once

#include "engine/manager/buffer.h"
#include <glm/gtc/type_ptr.hpp>
#include <cassert>


template<typename T, typename U>
static constexpr size_t offsetOf(U T::* member) {
    return (char*)&(static_cast<T*>(nullptr)->*member) - static_cast<char*>(nullptr); // NOLINT //-V2533
}

struct MeshGL final : OpenGLObject {
    friend struct MeshBuilderManager;

    MeshGL() = default;
    ~MeshGL() override {
        RENDER_ONLY_RENDER_THREAD
        glDeleteVertexArrays(1, &m_id);
        if (m_id) {
            TracyPlot("MeshGL", --m_count);
        }
    }

    MeshGL(const MeshGL& other)            = delete;
    MeshGL& operator=(const MeshGL& other) = delete;

    MeshGL(MeshGL&& other) noexcept
      : OpenGLObject(std::move(other))
      , m_IBO(std::move(other.m_IBO))
      , m_VBO(std::move(other.m_VBO))
      , m_PBO(std::move(other.m_PBO)) {}

    MeshGL& operator=(MeshGL&& other) noexcept {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, std::move(other));
        return *this;
    }

    static void unbind() {
        RENDER_ONLY_RENDER_THREAD
        glBindVertexArray(0);
    }

    void bind() const {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id);
        glBindVertexArray(m_id);
    }

    void setPositions(const std::vector<glm::mat4>& coords) {
        ZoneScoped;
        m_PBO.set<GL_DYNAMIC_DRAW>(coords);
    }
    void updatePositions(std::size_t index, const glm::mat4* const coords) {
        ZoneScoped;
        m_PBO.set(index, coords);
    }
    GLuint id() const { return m_id; }

    template<GLenum Buffer, typename T>
    requires(std::is_trivially_copyable_v<T> && Buffer == GL_ARRAY_BUFFER)
    void setData(const std::vector<T>& data) {
        ZoneScoped;
        bind();
        m_VBO.set(data);
    }

    template<GLenum Buffer, typename T>
    requires(std::is_trivially_copyable_v<T> && Buffer == GL_ELEMENT_ARRAY_BUFFER)
    void setData(const std::vector<T>& data) {
        ZoneScoped;
        bind();
        m_IBO.set(data);
    }


    template<typename T, typename U>
    requires std::is_trivially_copyable_v<T> && std::is_trivially_copyable_v<U>
    void setData(const std::vector<T>& vertex_buffer, const std::vector<U>& index_buffer) {
        ZoneScoped;
        bind();
        m_VBO.set(vertex_buffer);
        m_IBO.set(index_buffer);
    }

    template<typename T, typename FieldType, GLenum Elem = GL_FLOAT>
    requires(std::is_standard_layout_v<FieldType> && FieldType::length() > 0 && FieldType::length() < 5 &&
             sizeof(FieldType) == sizeof(typename FieldType::value_type) * FieldType::length())
    void setAttribPointer(GLuint index, FieldType T::* offset, GLuint divisor = 0, GLboolean normalize = GL_FALSE) {
        ZoneScoped;
        assert(index <= 15 && "out of range");
        checkTypes<FieldType, Elem>();

        bind();
        if (divisor) {
            m_PBO.bind();
        }

        glEnableVertexAttribArray(index);
        glVertexAttribPointer(
          index, FieldType::length(), Elem, normalize, sizeof(T), (void*)offsetOf(offset)); // NOLINT //-V2533

        glVertexAttribDivisor(index, divisor);
    }

    template<typename T, typename FieldType, GLenum Elem = GL_FLOAT>
    requires(std::is_standard_layout_v<T> && std::is_standard_layout_v<FieldType> && FieldType::length() > 0 &&
             FieldType::length() < 5 && sizeof(FieldType) == sizeof(typename FieldType::row_type) * FieldType::length())
    void setAttribPointer(GLuint index, FieldType T::* offset, GLuint divisor = 0, GLboolean normalize = GL_FALSE) {
        ZoneScoped;
        assert(index <= 16 - FieldType::length() && "out of range");
        checkTypes<FieldType, Elem>();

        bind();
        if (divisor) {
            m_PBO.bind();
        }

        for (int i = 0; i < FieldType::length(); ++i) {
            ZoneScopedN("Set one attrib pointer");
            glEnableVertexAttribArray(index + i);
            glVertexAttribPointer(
              index + i,
              FieldType::length(),
              Elem,
              normalize,
              sizeof(T),
              (void*)(offsetOf(offset) + sizeof(typename FieldType::row_type) * i)); // NOLINT //-V2533

            glVertexAttribDivisor(index + i, divisor);
        }
    }

    void draw(GLenum mode = GL_TRIANGLES) const {
        ZoneScoped;
        if (m_PBO.empty()) {
            return;
        }

        bind();
        if (m_IBO.empty()) {
            glDrawArraysInstanced(mode, 0, m_VBO.size(), m_PBO.size());
        } else {
            glDrawElementsInstanced(mode, m_IBO.size(), GL_UNSIGNED_INT, nullptr, m_PBO.size());
        }
    }


private:
    MeshGL(ElementBuffer ibo, ArrayBuffer vbo, ArrayBuffer pbo)
      : m_IBO(std::move(ibo)), m_VBO(std::move(vbo)), m_PBO(std::move(pbo)) {
        RENDER_ONLY_RENDER_THREAD
        glGenVertexArrays(1, &m_id);
        assert(m_id);
        TracyPlot("MeshGL", ++m_count);
    }


    template<typename FieldType, GLenum Elem = GL_FLOAT>
    void checkTypes() {
        switch (Elem) {
        case GL_FLOAT:
            assert((std::is_same_v<typename FieldType::value_type, float>));
            break;
        case GL_INT:
            assert((std::is_same_v<typename FieldType::value_type, int>));
            break;
        case GL_UNSIGNED_INT:
            assert((std::is_same_v<typename FieldType::value_type, unsigned int>));
            break;
        case GL_BYTE:
            assert((std::is_same_v<typename FieldType::value_type, char>));
            break;
        default:
            assert(false);
            break;
        }
    }

private:
    ElementBuffer m_IBO;
    ArrayBuffer   m_VBO;
    ArrayBuffer   m_PBO;
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};


struct MeshBuilderManager final {
    MeshGL create() { //-V2565
        ZoneScoped;
        return {BUFFER_MANAGER.create<GL_ELEMENT_ARRAY_BUFFER>(),
                BUFFER_MANAGER.create<GL_ARRAY_BUFFER>(),
                BUFFER_MANAGER.create<GL_ARRAY_BUFFER>()};
    }
};

#define MESH_MANAGER Singleton<MeshBuilderManager>::instance()
