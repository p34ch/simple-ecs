#pragma once

#include <sqlitefs/sqlitefs.h>

struct FileSystem final : SQLiteFS {
    using Parent    = SQLiteFS;
    using DataInput = std::vector<Data>;

    FileSystem(const std::string& path = "./data/gamefs.db");
    ~FileSystem() override;

    DataOutput read(const std::string& name) const;
    void       write(const std::string& name, DataInput data, const std::string& compression = "lzma");


private:
    void showUI();
    void showTree(const std::string& folder);
    void showTable();
    void createFolder();
    void importFile();
    void exportFile(const std::string& name);
    void mapFile(const std::string& name, const std::string& compression);


private:
    mutable std::atomic_uint32_t m_file_operation = 0;
    bool                         m_clicked        = false;
    std::string                  m_mapped_path    = "./mapped";
    std::string                  m_target_path    = "/";


private:
    struct FileWatcher;
    std::unique_ptr<FileWatcher> m_watcher;
};

#define FILE_SYSTEM Singleton<FileSystem>::instance()
