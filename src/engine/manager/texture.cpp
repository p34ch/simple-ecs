#include "engine/manager/texture.h"
#include "engine/manager/filesystem.h"
#include "engine/render/game_render.h" // IWYU pragma: exports
#include <filesystem>
#include <stb_image.h>


TextureBuilderManager::TextureBuilderManager() {
    RENDER_ADD_MEMBER_WINDOW(showUI);
};

TextureData TextureBuilderManager::loadTextureFromFile(const std::string& path) {
    ZoneScoped;
    TracyMessageStr("loading texture: " + path);

    if (std::filesystem::exists(path)) {
        int   width = 0, height = 0, nr_channels = 0; // NOLINT
        auto* ptr  = stbi_load(path.c_str(), &width, &height, &nr_channels, 0);
        auto  data = ImageData(ptr, stbi_image_free);

        return buildTextureData(width, height, nr_channels, std::move(data));
    }

    return loadTextureFromMemory(FILE_SYSTEM.read(path));
}

TextureData TextureBuilderManager::loadTextureFromMemory(std::span<const char> textute_data) {
    ZoneScoped;
    TracyMessageStr("loading texture from memory");

    int   width = 0, height = 0, nr_channels = 0; // NOLINT
    auto* ptr = stbi_load_from_memory(
      reinterpret_cast<const stbi_uc*>(textute_data.data()), textute_data.size(), &width, &height, &nr_channels, 0);
    auto data = ImageData(ptr, stbi_image_free);

    return buildTextureData(width, height, nr_channels, std::move(data));
}

TextureData TextureBuilderManager::buildTextureData(unsigned int width,
                                                    unsigned int height,
                                                    unsigned int nr_channels,
                                                    ImageData    data) {
    ZoneScoped;

    if (!data) {
        spdlog::error("Failed to load texture");
        return {};
    }

    GLenum format = 0;
    switch (nr_channels) {
    case STBI_grey:
        format = GL_RED;
        break;
    case STBI_grey_alpha:
        format = GL_RG;
        break;
    case STBI_rgb:
        format = GL_RGB;
        break;
    case STBI_rgb_alpha:
        format = GL_RGBA;
        break;
    default:
        spdlog::warn("Unnamed texture format");
        break;
    }
    auto        view = std::span{data.get(), static_cast<size_t>(width * height * nr_channels)}; //-V1028
    TextureData texture_data{.width = width, .height = height, .format = format, .data = {view.begin(), view.end()}};

    return texture_data;
}

void TextureBuilderManager::showUI() {
    constexpr std::string_view name = "Textures";

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Render")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }


    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        std::lock_guard lock{m_mutex};

        if (ImGui::Button("Clear")) {
            flush();
        }

        auto keys = TmpBuffer::get<std::vector<std::string>>();
        std::ranges::copy(std::views::keys(m_textures), std::back_inserter(*keys));
        std::ranges::sort(*keys);

        for (const auto& name : *keys) {
            if (ImGui::CollapsingHeader(name.empty() ? "NONAME" : name.data(), nullptr)) {
                const auto& texture = m_textures[name];
                ImGui::Text("use count: %d", texture.useCount());
                ImGui::Image(texture.get(), {256, 256}, {0, 1}, {1, 0});
            }
        }
    }
};

void TextureBuilderManager::flush() {
    ZoneScoped;

    tasks::start<tasks::Priority::CRITICAL>([this] {
        tracy::SetThreadName("TASKS Thread");

        std::lock_guard _{m_mutex};
        ZoneScopedN("clear unused textures");

        PROFILE_ONLY(auto size = m_textures.size());

        for (auto it = m_textures.begin(); it != m_textures.end();) {
            if (it->second.useCount() == 1) {
                m_scheduled_to_remove.emplace_back(std::move(it->second));

                RENDER_DISPATCH_END_FRAME_SLICED([this] {
                    std::lock_guard _{m_mutex};
                    assert(!m_scheduled_to_remove.empty());
                    m_scheduled_to_remove.pop_back();
                });

                it = m_textures.erase(it);
            } else {
                ++it;
            }
        }

        PROFILE_ONLY(size = size - m_textures.size());
        PROFILE_ONLY(TracyMessageStr("textures were removed: " + std::to_string(size)));
    });
}
