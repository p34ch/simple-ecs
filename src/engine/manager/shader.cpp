#include "engine/manager/shader.h"
#include "engine/components/register.h"
#include "engine/render/game_render.h" // IWYU pragma: exports


void ShaderGL::compile() {
    RENDER_ONLY_RENDER_THREAD
    ZoneScoped;

    const auto& [vertex, fragment] = SHADER(m_name);

    auto vert = compile<GL_VERTEX_SHADER>(vertex);
    auto frag = compile<GL_FRAGMENT_SHADER>(fragment);

    if (!m_id) {
        m_id = glCreateProgram();
        TracyPlot("ShaderGL", ++m_count);
    }

    DeferredCall cleanup([id = m_id, vert, frag] {
        glDetachShader(id, vert);
        glDetachShader(id, frag);
        glDeleteShader(vert);
        glDeleteShader(frag);
    });

    int success = 0;

    glAttachShader(m_id, vert);
    glAttachShader(m_id, frag);
    glLinkProgram(m_id);
    glGetProgramiv(m_id, GL_LINK_STATUS, &success);
    if (!success) {
        std::array<char, 1024> log{};
        glGetProgramInfoLog(m_id, log.size(), nullptr, log.data());
        spdlog::critical("Error. shader. compilation_failed: {}", log.data());
        return;
    }

    glValidateProgram(m_id);
    glGetProgramiv(m_id, GL_VALIDATE_STATUS, &success);
    if (!success) {
        std::array<char, 1024> log{};
        glGetProgramInfoLog(m_id, log.size(), nullptr, log.data());
        spdlog::critical("Error. Invalid shader program: {}", log.data());
        return;
    }

    spdlog::debug("Compiled: {}", m_name);
}

void ShaderGL::clean() {
    RENDER_ONLY_RENDER_THREAD

    if (m_id) {
        glDeleteProgram(m_id);
        spdlog::debug("Unload shader {} {}", m_id, m_name);
        TracyPlot("ShaderGL", --m_count);
    }
}

void ShaderGL::reload() {
    RENDER_ONLY_RENDER_THREAD
    compile();
}

void ShaderGL::setName(const std::string& name) {
    RENDER_ONLY_RENDER_THREAD

    m_name = name;
    reload();
}


ShaderBuilderManager::ShaderBuilderManager() {
    RENDER_ADD_MEMBER_WINDOW(showUI);
}

Shader ShaderBuilderManager::create(const std::string& name, [[maybe_unused]] bool debug) {
    RENDER_ONLY_RENDER_THREAD
    ZoneScoped;

    // create a new debug shader and exclude from the cache
    ECS_DEBUG_ONLY(if (debug) { return {name}; });

    auto pos = m_shaders.find(name);
    if (pos != m_shaders.end()) {
        return pos->second;
    }

    CountedGLObject<ShaderGL> shader{name};
    if (name != EMPTY) {
        m_shaders.emplace(name, shader); //-V837
    }

    return shader;
}

void ShaderBuilderManager::flush() {
    for (auto it = m_shaders.begin(); it != m_shaders.end();) {
        if (it->second.useCount() == 1) {
            it = m_shaders.erase(it);
        } else {
            ++it;
        }
    }
}

void ShaderBuilderManager::showUI() {
    constexpr std::string_view name = "Shaders";

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Render")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    constexpr ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY;
    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        if (ImGui::Button("Clear")) {
            flush();
        }

        if (ImGui::ScopedBeginTable("ShadersTable", 2, flags)) {
            ImGui::TableSetupScrollFreeze(0, 1); // Make top row always visible
            ImGui::TableSetupColumn("Name");
            ImGui::TableSetupColumn("Use", ImGuiTableColumnFlags_WidthFixed, 0.0f);
            ImGui::TableHeadersRow();

            for (const auto& [name, shader] : m_shaders) {
                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                ImGui::Selectable(name.c_str(), false, ImGuiSelectableFlags_SpanAllColumns);

                ImGui::TableNextColumn();
                ImGui::Text("%d", shader.useCount());
            }
        }
    }
}
