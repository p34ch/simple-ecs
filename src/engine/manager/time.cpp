#include "engine/manager/time.h"
#include "engine/render/game_render.h"

TimeManager::TimeManager() {
    RENDER_ADD_MEMBER_WINDOW(showUI);
}

void TimeManager::update() {
    m_frame++;

    double current_frame_time_point = glfwGetTime();
    m_delta_time                    = (current_frame_time_point - m_last_frame_time_point) * m_multiplier;
    m_last_frame_time_point         = current_frame_time_point;
}

void TimeManager::showUI() {
    constexpr std::string_view name = "Speed";

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Debug")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        ImGui::Text("Select game speed");
        ImGui::Text("Total frames: %zu", m_frame);
        ImGui::SliderFloat("Speed multiplier", &m_multiplier, 0.25F, 5.F, "%.2f");
        if (ImGui::Button("Reset")) {
            m_multiplier = 1.F;
        }
    }
}
