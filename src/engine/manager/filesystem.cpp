#include <filesystem>
#include <utility>

#include "engine/manager/filesystem.h"
#include "engine/render/game_render.h" // IWYU pragma: exports
#include <efsw/efsw.hpp>
#include <nfd.h>


struct FileSystem::FileWatcher final : public efsw::FileWatchListener {
    using Callback = std::function<void()>;

    FileWatcher(const std::string& path) : m_watch_id(m_watcher.addWatch(path, this, true)) { m_watcher.watch(); }
    ~FileWatcher() override { m_watcher.removeWatch(m_watch_id); }

    void addSubscription(const std::string& path, Callback callback) {
        std::lock_guard _(m_mutex);
        assert(!m_subscriptions.contains(path));
        m_subscriptions.emplace(path, callback);
    }

    void removeSubscription(const std::string& path) {
        std::lock_guard _(m_mutex);
        assert(m_subscriptions.contains(path));
        m_subscriptions.erase(path);
    }

    bool isSubscribed(const std::string& path) {
        std::lock_guard _(m_mutex);
        return m_subscriptions.contains(path);
    }

    void handleFileAction(efsw::WatchID /*watchid*/,
                          const std::string& dir,
                          const std::string& filename,
                          efsw::Action       action,
                          std::string /*oldFilename*/) override {
        if (action != efsw::Actions::Modified) {
            return;
        }

        std::lock_guard _(m_mutex);
        for (const auto& [path, callback] : m_subscriptions) {
            if (std::filesystem::equivalent(path, dir + filename)) {
                spdlog::debug("DIR ({}) FILE ({}) has event Modified", dir, filename);
                std::invoke(callback);
            }
        }
    }

private:
    efsw::FileWatcher                         m_watcher;
    efsw::WatchID                             m_watch_id;
    std::mutex                                m_mutex;
    std::unordered_map<std::string, Callback> m_subscriptions;
};

FileSystem::FileSystem(const std::string& path)
  : SQLiteFS(path), m_watcher(std::make_unique<FileWatcher>(m_mapped_path)) {
    RENDER_ADD_MEMBER_WINDOW(showUI);

    registerSaveFunc("mylz", [this](Parent::DataInput data) -> Parent::DataOutput {
        ZoneScoped;
        spdlog::stopwatch sw;

        constexpr size_t               size = 4ull * 1024 * 1024;
        std::vector<Parent::DataInput> parts;
        parts.reserve((data.size() / size) + 1);

        for (size_t offset = 0; offset < data.size(); offset += size) {
            size_t part_size = std::min(size, data.size() - offset);
            parts.emplace_back(data.subspan(offset, part_size));
        }

        auto tasks = tasks::parallel<tasks::Priority::IDLE>(std::move(parts),
                                                            [this](Parent::DataInput part) //
                                                            { return callSaveFunc("lzma", part); });

        Parent::DataOutput result;
        result.reserve(data.size());

        for (auto& task : tasks) {
            auto   data            = task.get();
            size_t compressed_size = data.size();
            auto   serialized_size = serializer::serialize(compressed_size);
            result.insert(result.end(), serialized_size.begin(), serialized_size.end());
            result.insert(result.end(), data.begin(), data.end());
        }

        spdlog::debug("Compressed {} bytes in {:.3}s", data.size(), sw);

        return result;
    });

    registerLoadFunc("mylz", [this](Parent::DataInput data) -> Parent::DataOutput {
        ZoneScoped;
        spdlog::stopwatch sw;

        std::vector<Parent::DataInput> parts;
        for (const auto* data_ptr = data.data(); data_ptr < data.data() + data.size();) {
            auto compressed_size = serializer::deserialize<size_t>(data_ptr);
            auto part            = std::span(data_ptr, compressed_size);
            parts.emplace_back(part);
            data_ptr += compressed_size;
        }

        auto tasks = tasks::parallel<tasks::Priority::IDLE>(std::move(parts),
                                                            [this](Parent::DataInput part) //
                                                            { return callLoadFunc("lzma", part); });

        std::vector<Parent::DataOutput> decompressed_parts;
        size_t                          decompressed_size = 0;
        for (auto& task : tasks) {
            decompressed_size += decompressed_parts.emplace_back(task.get()).size();
        }

        Parent::DataOutput result;
        result.reserve(decompressed_size);
        for (const auto& part : decompressed_parts) {
            result.insert(result.end(), part.begin(), part.end());
        }

        spdlog::debug("Decompressed {} bytes in {:.3}s", decompressed_size, sw);

        return result;
    });
}

FileSystem::~FileSystem() = default;


FileSystem::DataOutput FileSystem::read(const std::string& name) const {
    ZoneScoped;

    if (name.empty()) {
        return {};
    }

    m_file_operation.fetch_add(1, std::memory_order_relaxed);
    auto data = SQLiteFS::read(name);
    m_file_operation.fetch_sub(1, std::memory_order_relaxed);

    if (!data.empty()) {
        return data;
    }

    spdlog::warn("File not found: {}", name);
    return {};
}

void FileSystem::write(const std::string& name, DataInput data, const std::string& compression) {
    ZoneScoped;

    if (name.empty() || data.empty()) {
        return;
    }

    m_file_operation.fetch_add(1, std::memory_order_relaxed);
    tasks::start<tasks::Priority::IDLE>([this, name, data = std::move(data), compression] {
        if (SQLiteFS::write(name, data, compression)) {
            spdlog::info("File saved: {}", name);
        } else {
            spdlog::warn("File not saved: {}", name);
        }
        m_file_operation.fetch_sub(1, std::memory_order_relaxed);
    });
}

void FileSystem::showTree(const std::string& folder) {
    ZoneScoped;

    static ImGuiTreeNodeFlags base_flags =
      ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;

    std::unordered_map<std::string, SQLiteFSNode> tree;
    for (const auto& node : ls(folder)) {
        tree[folder + "/" + node.name] = node;
    }

    for (const auto& [name, node] : tree) {
        if (!(node.attributes & SQLiteFSNode::FILE)) {
            bool is_clicked = false;

            ImGui::TableNextRow();
            ImGui::TableNextColumn();

            if (ImGui::TreeNodeEx(node.name.c_str(), base_flags)) {
                is_clicked |= ImGui::IsItemClicked();

                showTree(name);
                ImGui::TreePop();
            }

            is_clicked |= ImGui::IsItemClicked();
            if (is_clicked && !m_clicked) {
                cd(name);
                m_clicked = true;
            }
        }
    }
}

void FileSystem::showTable() {
    ZoneScoped;

    constexpr ImGuiTableFlags flags = ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg | ImGuiTableFlags_ScrollY;
    if (ImGui::ScopedBeginTable("Current working directory", 8, flags)) {
        ImGui::TableSetupScrollFreeze(0, 1); // Make top row always visible
        ImGui::TableSetupColumn("Name");
        ImGui::TableSetupColumn("Size (C)", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Size (R)", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Alg", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Cp", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Exp", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Map", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableSetupColumn("Del", ImGuiTableColumnFlags_WidthFixed, 0.0f);
        ImGui::TableHeadersRow();

        auto             items = ls();
        ImGuiListClipper clipper;
        clipper.Begin(items.size());

        while (clipper.Step()) {
            for (int row_n = clipper.DisplayStart; row_n < clipper.DisplayEnd; row_n++) {
                const auto& node = items[row_n];
                ImGui::TableNextRow();
                ImGui::TableNextColumn();
                ImGui::Selectable(
                  node.name.c_str(), false, ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowOverlap);

                if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left) &&
                    !(node.attributes & SQLiteFSNode::Attributes::FILE)) {
                    cd(node.name);
                }

                auto calc_size = [](std::uint64_t size) {
                    int  index    = 0;
                    auto size_raw = static_cast<float>(size);
                    while (size_raw > 1024) {
                        size_raw /= 1024;
                        index++;
                    }
                    return std::format("{:.1f} {}", size_raw, "BKMGT"[index]);
                };

                ImGui::TableNextColumn();
                ImGui::Text("%s", calc_size(node.size).c_str());
                ImGui::TableNextColumn();
                ImGui::Text("%s", calc_size(node.size_raw).c_str());
                ImGui::TableNextColumn();
                ImGui::Text("%s", node.compression.c_str());

                ImGui::ScopedPushID(row_n);
                {
                    ImGui::ScopedBeginDisabled(!(node.attributes & SQLiteFSNode::Attributes::FILE));
                    {
                        ImGui::TableNextColumn();
                        ImGui::ScopedPushStyleColor(ImGuiCol_Button, ImVec4(0.00f, 0.50f, 0.50f, 1.00f));
                        if (ImGui::SmallButton("\uf4bb")) {
                            tasks::start([this, name = node.name] {
                                m_file_operation.fetch_add(1, std::memory_order_relaxed);
                                cp(name, m_target_path + "/" + name);
                                m_file_operation.fetch_sub(1, std::memory_order_relaxed);
                            });
                        }

                        ImGui::TableNextColumn();
                        ImGui::ScopedPushStyleColor(ImGuiCol_Button, ImVec4(0.50f, 0.00f, 0.50f, 1.00f));
                        if (ImGui::SmallButton("\uf1c0")) {
                            exportFile(node.name);
                        }
                    }

                    ImGui::TableNextColumn();
                    auto filename      = m_mapped_path + pwd() + "/" + node.name;
                    bool is_subscribed = m_watcher->isSubscribed(filename);
                    ImGui::ScopedPushStyleColor(ImGuiCol_Button,
                                                is_subscribed ? ImVec4(0.050f, 0.50f, 0.00f, 1.00f) :
                                                                ImVec4(0.50f, 0.00f, 0.00f, 1.00f));

                    if (ImGui::SmallButton("\uf481")) {
                        is_subscribed ? m_watcher->removeSubscription(filename) : mapFile(node.name, node.compression);
                    }
                }

                {
                    ImGui::TableNextColumn();
                    ImGui::ScopedPushStyleColor(ImGuiCol_Button, ImVec4(0.50f, 0.00f, 0.00f, 1.00f));
                    if (ImGui::SmallButton("\uf4d6")) {
                        rm(node.name);
                    }
                }
            }
        }
    }
}

void FileSystem::showUI() {
    constexpr std::string_view name = "SQLiteFS";
    m_clicked                       = false;

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Tools")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_MenuBar)) {
        if (ImGui::ScopedBeginMenuBar()) {
            createFolder();
            importFile();

            if (ImGui::ScopedBeginMenu("Transfer")) {
                if (ImGui::MenuItem("Set target path")) {
                    m_target_path = pwd();
                }
            }

            if (ImGui::ScopedBeginMenu("Misc")) {
                if (ImGui::MenuItem("Vacuum")) {
                    tasks::start<tasks::Priority::CRITICAL>(&SQLiteFS::vacuum, this);
                }
            }

            ImGui::Separator();
            ImGui::Text("Tasks %d", m_file_operation.load(std::memory_order_relaxed));
            ImGui::Separator();
            ImGui::Text("Target: '%s'", m_target_path.c_str());
        }


        if (ImGui::ScopedBeginTable("ControlsTable", 3)) {
            ImGui::TableSetupColumn("Text", ImGuiTableColumnFlags_WidthFixed, 0.0f);
            ImGui::TableSetupColumn("Path");
            ImGui::TableSetupColumn("Buttons", ImGuiTableColumnFlags_WidthFixed, 0.0f);

            ImGui::TableNextRow();

            ImGui::TableNextColumn();
            ImGui::AlignTextToFramePadding();
            ImGui::TextUnformatted("Path");

            ImGui::TableNextColumn();
            auto path = pwd();
            {
                ImGui::ScopedPushItemWidth(-1);
                ImGui::InputText("##Path", &path, ImGuiInputTextFlags_ReadOnly);
            }

            ImGui::TableNextColumn();
            if (ImGui::Button("/")) {
                cd("/");
            }
            ImGui::SameLine();
            if (ImGui::Button("..")) {
                cd("..");
            }
        }

        ImGui::Separator();
        if (ImGui::ScopedBeginTable("NodesTable", 2, ImGuiTableFlags_BordersInnerV | ImGuiTableFlags_Resizable)) {
            ImGui::TableNextRow();

            ImGui::TableNextColumn();
            if (ImGui::BeginTable("##bg", 1, ImGuiTableFlags_ScrollY)) {
                showTree("");
                ImGui::EndTable();
            }

            ImGui::TableNextColumn();
            showTable();
        }
    }
}


void FileSystem::createFolder() {
    static std::string folder_name;
    bool               show = false;

    if (ImGui::ScopedBeginMenu("New")) {
        if (ImGui::MenuItem("New folder")) {
            show = true;
        }
    }

    if (show) {
        ImGui::OpenPopup("New folder");
    }

    if (ImGui::BeginPopupModal("New folder")) {
        ImGui::Text("Name");

        {
            ImGui::ScopedPushItemWidth(-1);
            ImGui::SameLine();
            ImGui::InputText("##Folder name", &folder_name);
        }

        if (ImGui::Button("OK")) {
            mkdir(folder_name);
            folder_name.clear();
            ImGui::CloseCurrentPopup();
        }
        ImGui::SameLine();
        if (ImGui::Button("Cancel")) {
            folder_name.clear();
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void FileSystem::importFile() {
    static std::string file;
    static bool        compressed = false;
    bool               show       = false;

    if (ImGui::ScopedBeginMenu("New")) {
        if (ImGui::MenuItem("Import file")) {
            show = true;
        }
    }

    if (show) {
        ImGui::OpenPopup("Import file");
    }


    ImGui::SetNextWindowSize({320, 115});
    if (ImGui::BeginPopupModal("Import file", nullptr, ImGuiWindowFlags_NoResize)) {
        ImGui::Text("File");

        {
            ImGui::ScopedPushItemWidth(250);
            ImGui::SameLine();
            ImGui::InputText("##Filepath", &file);
        }

        ImGui::SameLine();
        if (ImGui::Button("...")) {
            tasks::start([] {
                if (NFD_Init() != NFD_OKAY) {
                    spdlog::error("Error: {0}", NFD_GetError());
                    return;
                }
                nfdu8char_t* filepath = nullptr;
                nfdresult_t  result   = NFD_OpenDialog(&filepath, nullptr, 0, nullptr);
                if (result == NFD_OKAY) {
                    spdlog::debug("Import {}", filepath);
                    file = filepath;
                    NFD_FreePathU8(filepath);
                } else if (result == NFD_CANCEL) {
                    spdlog::debug("Import was cancelled");
                } else {
                    spdlog::error("Error: {0}", NFD_GetError());
                }
                NFD_Quit();
            });
        }

        ImGui::Checkbox("Compress", &compressed);
        ImGui::Separator();

        if (ImGui::Button("OK")) {
            m_file_operation.fetch_add(1, std::memory_order_relaxed);
            tasks::start<tasks::Priority::IDLE>([this, file = std::move(file)] {
                std::ifstream is(file, std::ifstream::binary);
                DataInput     buffer;
                if (is) {
                    buffer = DataInput(std::istreambuf_iterator<char>(is), {});
                }
                is.close();
                m_file_operation.fetch_sub(1, std::memory_order_relaxed);

                if (!buffer.empty()) {
                    auto filename = std::filesystem::path(file).filename().string();
                    write(filename, std::move(buffer), compressed ? "mylz" : "raw");
                }
            });

            ImGui::CloseCurrentPopup();
        }

        ImGui::SameLine();
        if (ImGui::Button("Cancel")) {
            file.clear();
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void FileSystem::exportFile(const std::string& name) {
    tasks::start<tasks::Priority::IDLE>([name, this] {
        if (NFD_Init() != NFD_OKAY) {
            spdlog::error("Error: {0}", NFD_GetError());
            return;
        }
        nfdu8char_t* filepath = nullptr;
        nfdresult_t  result   = NFD_SaveDialog(&filepath, nullptr, 0, nullptr, name.c_str());
        if (result == NFD_OKAY) {
            std::string path = filepath;
            spdlog::debug("Export {}", path);

            tasks::start<tasks::Priority::IDLE>([this, name, path = std::move(path)]() mutable {
                std::ofstream fs(path, std::ios::out | std::ios::binary);
                if (fs) {
                    auto bytes = read(name);
                    m_file_operation.fetch_add(1, std::memory_order_relaxed);
                    fs.write(reinterpret_cast<const char*>(bytes.data()), bytes.size());
                    fs.close();
                    m_file_operation.fetch_sub(1, std::memory_order_relaxed);
                } else {
                    spdlog::error("Can't open file {}", path);
                }
            });

            NFD_FreePathU8(filepath);
        } else if (result == NFD_CANCEL) {
            spdlog::debug("Export was cancelled");
        } else {
            spdlog::error("Error: {0}", NFD_GetError());
        }
        NFD_Quit();
    });
}

void FileSystem::mapFile(const std::string& name, const std::string& compression) {
    tasks::start<tasks::Priority::IDLE>([this, name, compression] {
        auto db_path     = pwd();
        auto fs_path     = m_mapped_path + db_path;
        auto fs_filename = fs_path + "/" + name;

        auto data = tasks::start<tasks::Priority::IDLE>([this, name] { return read(name); });

        std::filesystem::create_directories(fs_path);
        std::ofstream fs(fs_filename, std::ios::out | std::ios::binary);

        if (!fs) {
            spdlog::error("Can't open file {}", fs_filename);
            return;
        }

        auto bytes = data.get();
        fs.write(reinterpret_cast<const char*>(bytes.data()), bytes.size());
        fs.close();

        auto cb = [this, name, compression, fs_path = std::move(fs_path), db_path = std::move(db_path)] {
            auto fs_filename = fs_path + "/" + name;
            auto db_name     = db_path + "/" + name;

            tasks::start<tasks::Priority::IDLE>([this, compression, fs_filename, db_name] {
                std::ifstream is(fs_filename, std::ifstream::binary);
                if (is) {
                    DataInput buffer(std::istreambuf_iterator<char>(is), {});
                    rm(db_name);
                    write(db_name, std::move(buffer), compression);
                }
                is.close();
            });
        };

        using namespace std::chrono_literals;
        std::this_thread::sleep_for(500ms);
        m_watcher->addSubscription(fs_filename, std::move(cb));
    });
}
