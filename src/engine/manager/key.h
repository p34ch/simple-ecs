#pragma once

struct KeyboardManager final {
    void setKey(size_t code, bool value);
    bool key(size_t code);
    void reset();
    void setEnabled(bool value);

private:
    std::array<bool, GLFW_KEY_LAST + 1> m_keys{};
    bool                                m_enabled = true;
};

#define KEYBOARD_MANAGER Singleton<KeyboardManager>::instance()
