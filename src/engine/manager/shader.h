#pragma once

#include "engine/manager/filesystem.h"
#include <glm/gtc/type_ptr.hpp>
#include "common/opengl_object.h"
#include <utility>


struct ShaderGL final : OpenGLObject {
    friend struct CountedGLObject<ShaderGL>;

    ShaderGL()           = default;
    ~ShaderGL() override = default;

    ShaderGL(const ShaderGL& other)                = default;
    ShaderGL(ShaderGL&& other) noexcept            = default;
    ShaderGL& operator=(const ShaderGL& other)     = delete;
    ShaderGL& operator=(ShaderGL&& other) noexcept = delete;

    const std::string& name() const noexcept { return m_name; }
    void               bind() const {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id);
        glUseProgram(m_id);
    }

    ECS_DEBUG_ONLY(bool isLiveUpdate() const noexcept { return m_live_update; })
    ECS_DEBUG_ONLY(void setLiveUpdate(bool b) noexcept { m_live_update = b; })

    GLuint getUniformID(const char* name) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        return glGetUniformLocation(m_id, name);
    }

    // matrix
    void load(const char* name, const glm::mat4& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniformMatrix4fv(getUniformID(name), 1, GL_FALSE, glm::value_ptr(data));
    }
    void load(const char* name, const glm::mat3& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniformMatrix3fv(getUniformID(name), 1, GL_FALSE, glm::value_ptr(data));
    }
    void load(const char* name, const glm::mat2& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniformMatrix2fv(getUniformID(name), 1, GL_FALSE, glm::value_ptr(data));
    }
    // vector
    void load(const char* name, const glm::vec4& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform4f(getUniformID(name), data[0], data[1], data[2], data[3]);
    }
    void load(const char* name, const glm::vec3& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform3f(getUniformID(name), data[0], data[1], data[2]);
    }
    void load(const char* name, const glm::vec2& data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform2f(getUniformID(name), data[0], data[1]);
    }

    // simple
    void load(const char* name, bool data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform1ui(getUniformID(name), data);
    }
    void load(const char* name, std::size_t data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform1ui(getUniformID(name), data);
    }
    void load(const char* name, GLint data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform1i(getUniformID(name), data);
    }
    void load(const char* name, GLfloat data) const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glUniform1f(getUniformID(name), data);
    }

    void reload();
    void setName(const std::string& name);

private:
    ShaderGL(std::string name) : m_name(std::move(name)) { //-V2511
        RENDER_ONLY_RENDER_THREAD;
        compile();
    }

    void compile();
    void clean();

private:
    template<GLuint Type>
    requires(Type == GL_VERTEX_SHADER || Type == GL_FRAGMENT_SHADER)
    GLuint compile(const std::string& shader_file);

private:
    std::string m_name;
    ECS_DEBUG_ONLY(bool m_live_update = false);
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};


template<GLuint Type>
requires(Type == GL_VERTEX_SHADER || Type == GL_FRAGMENT_SHADER)
GLuint ShaderGL::compile(const std::string& shader_file) {
    RENDER_ONLY_RENDER_THREAD
    ZoneScoped;

    auto data = FILE_SYSTEM.read("/shaders/"_t + shader_file);
    assert(data.size());
    data.emplace_back(0); // we read raw text file, we need to add 0 at the end to have CStr
    const char* ptr = reinterpret_cast<const char*>(data.data());

    GLuint shader = glCreateShader(Type);
    glShaderSource(shader, 1, &ptr, nullptr);
    glCompileShader(shader);

    int success = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        std::array<char, 1024> log{};
        glGetProgramInfoLog(shader, log.size(), nullptr, log.data());
        spdlog::critical("Error. shader. compilation_failed: {}", log.data());
    }

    return shader;
}


struct ShaderBuilderManager final {
    ShaderBuilderManager();
    Shader create(const std::string& name, [[maybe_unused]] bool debug = false);
    void   flush();

private:
    void showUI();

private:
    std::unordered_map<std::string, Shader> m_shaders;
};

#define SHADER_MANAGER Singleton<ShaderBuilderManager>::instance()
