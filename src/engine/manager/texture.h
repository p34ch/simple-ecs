#pragma once

#include <glm/gtc/type_ptr.hpp>
#include "common/opengl_object.h"
#include <render/core/keep_alive_guard.h>


using ImageData = std::unique_ptr<unsigned char, void (*)(void*)>;
struct TextureData final {
    unsigned int               width, height;
    GLenum                     format;
    std::vector<unsigned char> data;
};

template<GLenum Type = GL_TEXTURE_2D>
struct TextureGL final : OpenGLObject {
    friend struct CountedGLObject<TextureGL>;

    static const GLenum type        = Type;
    static const GLuint max_texture = 32;

    TextureGL()                                      = default;
    TextureGL(const TextureGL& other)                = default;
    TextureGL& operator=(const TextureGL& other)     = delete;
    TextureGL(TextureGL&& other) noexcept            = default;
    TextureGL& operator=(TextureGL&& other) noexcept = delete;
    ~TextureGL() override                            = default;


    void bind(int layer = 0) const {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id);
        assert(layer < max_texture);

        glActiveTexture(GL_TEXTURE0 + layer);
        glBindTexture(type, m_id);
    }

    static void unbind(int layer = 0) {
        RENDER_ONLY_RENDER_THREAD
        assert(layer < max_texture);
        glActiveTexture(GL_TEXTURE0 + layer);
        glBindTexture(type, 0);
    }

    template<GLenum T>
    requires(type == GL_TEXTURE_2D)
    void setTexImage(int w, int h, GLenum InternalFormat = GL_RGB, GLenum Format = GL_RGB, const void* data = nullptr) {
        ZoneScoped;
        bind();
        glTexImage2D(type, 0, InternalFormat, w, h, 0, Format, T, data);
    }

    template<GLenum T>
    requires(type == GL_TEXTURE_CUBE_MAP)
    void setTexImage(int         offset,
                     int         w,
                     int         h,
                     GLenum      InternalFormat = GL_RGB,
                     GLenum      Format         = GL_RGB,
                     const void* data           = nullptr) {
        ZoneScoped;
        bind();
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + offset, 0, InternalFormat, w, h, 0, Format, T, data);
    }

    template<GLenum Filter>
    void setTexParam(GLint value) {
        ZoneScoped;
        bind();
        glTexParameteri(type, Filter, value);
    }

    template<GLenum Filter>
    void setTexParam(GLfloat value) {
        ZoneScoped;
        bind();
        glTexParameterf(type, Filter, value);
    }

    template<GLenum Filter>
    void setTexParam(glm::vec4 value) {
        ZoneScoped;
        bind();
        glTexParameterfv(type, Filter, glm::value_ptr(value));
    }

    template<GLenum Filter>
    void setTexParam(glm::ivec4 value) {
        ZoneScoped;
        bind();
        glTexParameteriv(type, Filter, glm::value_ptr(value));
    }

    void applyTextureData(const TextureData& texture_data)
    requires(type == GL_TEXTURE_2D)
    {
        ZoneScoped;
        TracyMessageStr("texture size (bytes): " + std::to_string(texture_data.data.size()));
        setTexImage<GL_UNSIGNED_BYTE>(
          texture_data.width, texture_data.height, texture_data.format, texture_data.format, texture_data.data.data());
        glGenerateMipmap(GL_TEXTURE_2D);

        setTexParam<GL_TEXTURE_WRAP_S>(GL_REPEAT);
        setTexParam<GL_TEXTURE_WRAP_T>(GL_REPEAT);
        setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR_MIPMAP_LINEAR);
        setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR);
    }

    void applyTextureData(const std::array<TextureData, 6>& texture_data)
    requires(type == GL_TEXTURE_CUBE_MAP)
    {
        ZoneScoped;
        for (unsigned int i = 0; i < texture_data.size(); i++) {
            ZoneScopedN("Set one texture");
            TracyMessageStr("texture size (bytes): " + std::to_string(texture_data[i].data.size()));
            setTexImage<GL_UNSIGNED_BYTE>(i,
                                          texture_data[i].width,
                                          texture_data[i].height,
                                          texture_data[i].format,
                                          texture_data[i].format,
                                          texture_data[i].data.data());
        }

        setTexParam<GL_TEXTURE_MIN_FILTER>(GL_LINEAR);
        setTexParam<GL_TEXTURE_MAG_FILTER>(GL_LINEAR);
        setTexParam<GL_TEXTURE_WRAP_S>(GL_CLAMP_TO_EDGE);
        setTexParam<GL_TEXTURE_WRAP_T>(GL_CLAMP_TO_EDGE);
        setTexParam<GL_TEXTURE_WRAP_R>(GL_CLAMP_TO_EDGE);
    }

    operator ImTextureID() const noexcept { return m_id; }


private:
    TextureGL(const std::string& /*dummy*/) {
        RENDER_ONLY_RENDER_THREAD
        ZoneScoped;
        glGenTextures(1, &m_id);
        assert(m_id);
        TracyPlot("TextureGL", ++m_count);
    }

    void clean() {
        ZoneScoped;
        RENDER_ONLY_RENDER_THREAD
        if (m_id) {
            glDeleteTextures(1, &m_id);
            spdlog::debug("Unload texture {}", m_id);
            TracyPlot("TextureGL", --m_count);
        }
    }


private:
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};


struct TextureBuilderManager final {
    TextureBuilderManager();

    template<GLuint Type = GL_TEXTURE_2D, bool Debug = false>
    CountedGLObject<TextureGL<Type>> create(std::string name) {
        ZoneScoped;

        if constexpr (Type == GL_TEXTURE_2D) {
            if constexpr (Debug) {
                static std::atomic_uint32_t counter = 0;
                auto                        buf     = TmpBuffer::get<std::string>();

                *buf = std::format("{:05}", counter.fetch_add(1, std::memory_order_relaxed));
                name = "DEBUG_" + *buf + " - " + name;
            }

            CountedGLObject<TextureGL<Type>> texture{name};
            bool                             emplaced = false;
            {
                std::lock_guard lock{m_mutex};
                std::tie(std::ignore, emplaced) = m_textures.try_emplace(name, texture);
            }
            if (!emplaced) {
                spdlog::warn("Texture already exists: {}", name);
            }

            return texture;
        } else {
            if constexpr (Debug) {
                compiletimeFail("Doesn't support");
            }

            return {name};
        }
    }

    template<GLuint Type = GL_TEXTURE_2D>
    requires(Type == GL_TEXTURE_2D)
    bool has(const std::string& name) {
        std::lock_guard lock{m_mutex};
        ZoneScoped;
        auto pos = m_textures.find(name);
        return pos != m_textures.end();
    }

    template<GLuint Type = GL_TEXTURE_2D>
    requires(Type == GL_TEXTURE_2D)
    CountedGLObject<TextureGL<Type>> get(const std::string& name) const {
        std::lock_guard lock{m_mutex};
        ZoneScoped;
        auto pos = m_textures.find(name);
        return pos != m_textures.end() ? pos->second : CountedGLObject<TextureGL<Type>>();
    }

    template<GLuint Type>
    requires(Type != GL_TEXTURE_2D)
    CountedGLObject<TextureGL<Type>> get() const {
        ZoneScoped;
        return CountedGLObject<TextureGL<Type>>();
    }

    TextureData loadTextureFromFile(const std::string& path);
    TextureData loadTextureFromMemory(std::span<const char> textute_data);

    void flush();

private:
    TextureData buildTextureData(unsigned int width, unsigned int height, unsigned int nr_channels, ImageData data);

    void showUI();

private:
    mutable TracyLockable(std::mutex, m_mutex);

    std::unordered_map<std::string, Texture2D> m_textures;
    std::vector<Texture2D>                     m_scheduled_to_remove;
};

#define TEXTURE_MANAGER Singleton<TextureBuilderManager>::instance()
