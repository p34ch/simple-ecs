#pragma once

#include "common/opengl_object.h"

template<GLenum Type>
struct BufferGL final : OpenGLObject {
    friend struct BufferBuilderManager;
    static const GLenum type = Type;

    BufferGL()                                 = default;
    BufferGL(const BufferGL& other)            = delete;
    BufferGL& operator=(const BufferGL& other) = delete;

    BufferGL(BufferGL&& other) noexcept : OpenGLObject(std::move(other)), m_size(std::exchange(other.m_size, 0)) {}

    BufferGL& operator=(BufferGL&& other) noexcept {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, std::move(other));
        return *this;
    }

    ~BufferGL() override {
        RENDER_ONLY_RENDER_THREAD
        glDeleteBuffers(1, &m_id);
        if (m_id) {
            TracyPlot("BufferGL", --m_count);
        }
    }

    static void unbind() {
        RENDER_ONLY_RENDER_THREAD
        glBindBuffer(type, 0);
    }

    void bind() {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id);
        glBindBuffer(type, m_id);
    }


    GLuint id() const noexcept { return m_id; }
    GLuint size() const noexcept { return m_size; }
    bool   empty() const noexcept { return !m_size; }

    template<GLenum usage = GL_STATIC_DRAW, typename T>
    requires std::is_trivially_copyable_v<T>
    void set(const std::vector<T>& data) {
        ZoneScoped;
        TracyMessageStr("buffer size (bytes): " + std::to_string(data.size()));
        bind();
        glBufferData(type, data.size() * sizeof(T), data.data(), usage);
        m_size = static_cast<GLuint>(data.size());
    }

    template<GLenum usage = GL_STATIC_DRAW, typename T>
    requires std::is_trivially_copyable_v<T>
    void allocate(std::size_t size) {
        ZoneScoped;
        TracyMessageStr("allocate empty size (bytes): " + std::to_string(size));
        bind();
        glBufferData(type, size * sizeof(T), nullptr, usage);
        m_size = size;
    }

    template<typename T>
    requires std::is_trivially_copyable_v<T>
    void set(std::size_t index, const T* const value) {
        ZoneScoped;
        TracyMessageStr("buffer size (bytes): " + std::to_string(sizeof(T)) + " at: " + std::to_string(index));
        assert(index < m_size && "Out of bounds");
        bind();
        glBufferSubData(type, index * sizeof(T), sizeof(T), value);
    }

    template<typename T>
    requires std::is_trivially_copyable_v<T>
    void set(std::size_t index, std::size_t count, const T* const from) {
        ZoneScoped;
        TracyMessageStr("buffer size (bytes): " + std::to_string(count * sizeof(T)) + " at: " + std::to_string(index));
        assert(index < m_size && "Out of bounds");
        assert(index + count < m_size && "Out of bounds");
        bind();
        glBufferSubData(type, index * sizeof(T), count * sizeof(T), from);
    }

    void bindBase(int index)
    requires(type == GL_UNIFORM_BUFFER)
    {
        RENDER_ONLY_RENDER_THREAD
        glBindBufferBase(type, index, m_id);
    }

private:
    void init() {
        RENDER_ONLY_RENDER_THREAD
        glGenBuffers(1, &m_id);
        assert(m_id);
        TracyPlot("BufferGL", ++m_count);
    }

private:
    GLuint m_size = 0;
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};


template<>
struct BufferGL<GL_FRAMEBUFFER> final : OpenGLObject {
    friend struct BufferBuilderManager;
    static const GLenum type = GL_FRAMEBUFFER;

    BufferGL()                                 = default;
    BufferGL(const BufferGL& other)            = delete;
    BufferGL& operator=(const BufferGL& other) = delete;

    BufferGL(BufferGL&& other) noexcept : OpenGLObject(std::move(other)) {}
    BufferGL& operator=(BufferGL&& other) noexcept {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, std::move(other));
        return *this;
    }

    ~BufferGL() override {
        RENDER_ONLY_RENDER_THREAD
        glDeleteFramebuffers(1, &m_id);
        if (m_id) {
            TracyPlot("BufferGL<GL_FRAMEBUFFER>", --m_count);
        }
    }

    GLuint id() const noexcept { return m_id; }

    static void unbind() {
        RENDER_ONLY_RENDER_THREAD
        glBindFramebuffer(type, 0);
    }

    void bind() const {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id); //-V3545
        glBindFramebuffer(type, m_id);
    }

    GLenum status() {
        RENDER_ONLY_RENDER_THREAD
        ZoneScoped;
        return glCheckFramebufferStatus(type);
    };

    template<GLenum Type>
    requires(Type == GL_DRAW_FRAMEBUFFER || Type == GL_READ_FRAMEBUFFER)
    void bind() const noexcept {
        RENDER_ONLY_RENDER_THREAD
        glBindFramebuffer(Type, m_id);
    }

    template<GLenum Attachment, GLenum TextureType>
    void setFrameBufferTexture(const TextureGL<TextureType>& texture) {
        ZoneScoped;
        bind<GL_DRAW_FRAMEBUFFER>();
        texture.bind();
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, Attachment, TextureType, texture.id(), 0);
    }

    template<GLenum Attachment, GLenum BufferType>
    void setFramebufferRenderbuffer(BufferGL<BufferType>& texture) {
        ZoneScoped;
        bind();
        texture.bind();
        glFramebufferRenderbuffer(type, Attachment, BufferType, texture.id());
    }

    template<GLenum T>
    void setRData() noexcept {
        ZoneScoped;
        bind<GL_READ_FRAMEBUFFER>();
        glReadBuffer(T);
    }

    template<GLenum T>
    void setWData(uint8_t count = 1) noexcept {
        ZoneScoped;
        std::vector<GLenum> data(count);
        for (int i = 0; i < count; i++) {
            data[i] = T + i;
        }

        bind<GL_DRAW_FRAMEBUFFER>();
        glDrawBuffers(count, data.data());
    }

    template<GLenum... Ts>
    requires(sizeof...(Ts) > 1)
    void setWData() noexcept {
        ZoneScoped;
        std::array<GLenum, sizeof...(Ts)> data = {Ts...};

        bind<GL_DRAW_FRAMEBUFFER>();
        glDrawBuffers(sizeof...(Ts), data.data());
    }

    template<GLenum T>
    void setRWData() noexcept {
        ZoneScoped;
        setRData<T>();
        setWData<T>();
    }

private:
    void init() {
        RENDER_ONLY_RENDER_THREAD
        glGenFramebuffers(1, &m_id);
        assert(m_id);
        TracyPlot("BufferGL<GL_FRAMEBUFFER>", ++m_count);
    }

private:
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};

template<>
struct BufferGL<GL_RENDERBUFFER> final : OpenGLObject {
    friend struct BufferBuilderManager;
    static const GLenum type = GL_RENDERBUFFER;

    BufferGL()                                 = default;
    BufferGL(const BufferGL& other)            = delete;
    BufferGL& operator=(const BufferGL& other) = delete;

    BufferGL(BufferGL&& other) noexcept : OpenGLObject(std::move(other)) {}
    BufferGL& operator=(BufferGL&& other) noexcept {
        if (this == &other) {
            return *this;
        }

        std::destroy_at(this);
        std::construct_at(this, std::move(other));
        return *this;
    }

    ~BufferGL() override {
        RENDER_ONLY_RENDER_THREAD
        glDeleteRenderbuffers(1, &m_id);
        if (m_id) {
            TracyPlot("BufferGL<GL_RENDERBUFFER>", --m_count);
        }
    }

    GLuint id() const noexcept { return m_id; }

    static void unbind() {
        RENDER_ONLY_RENDER_THREAD
        glBindRenderbuffer(type, 0);
    }

    void bind() const {
        RENDER_ONLY_RENDER_THREAD
        assert(m_id);
        glBindRenderbuffer(type, m_id);
    }

    void setSize(int w, int h) {
        ZoneScoped;
        bind();
        glRenderbufferStorage(type, GL_DEPTH24_STENCIL8, w, h);
    }

private:
    void init() {
        RENDER_ONLY_RENDER_THREAD
        glGenRenderbuffers(1, &m_id);
        assert(m_id);
        TracyPlot("BufferGL<GL_RENDERBUFFER>", ++m_count);
    }

private:
    PROFILE_ONLY(static inline std::atomic_int64_t m_count = 0;)
};


struct BufferBuilderManager {
    template<GLuint Type>
    BufferGL<Type> create() {
        ZoneScoped;
        BufferGL<Type> buffer;
        buffer.init();
        return buffer;
    }
};

#define BUFFER_MANAGER Singleton<BufferBuilderManager>::instance()