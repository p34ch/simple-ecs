#include "engine/manager/key.h"


void KeyboardManager::setKey(size_t code, bool value) {
    if (ECS_FINAL_SWITCH(false, !m_enabled)) {
        return;
    }

    if (code < m_keys.size()) {
        m_keys[code] = value;
    }
}

bool KeyboardManager::key(size_t code) {
    return m_keys[code];
}

void KeyboardManager::reset() {
    m_keys = {};
}

void KeyboardManager::setEnabled(bool value) {
    m_enabled = value;
    if (!m_enabled) {
        reset();
    }
}
