#include "engine/manager/mouse.h"
#include "engine/render/game_render.h" // IWYU pragma: export


MouseManager::MouseManager() {
    RENDER_ADD_MEMBER_WINDOW(showUI);
}

void MouseManager::setButton(size_t code, bool state) {
    if (ECS_FINAL_SWITCH(false, !m_enabled)) {
        return;
    }

    assert(code < m_buttons.size());
    m_buttons[code] = state;
}

bool MouseManager::button(size_t code) {
    return m_buttons[code];
}

void MouseManager::reset() {
    m_buttons = {};
}

void MouseManager::setEnabled(bool value) {
    m_enabled = value;
    if (!m_enabled) {
        reset();
    }
}

void MouseManager::setPosition(float x, float y) {
    if (ECS_FINAL_SWITCH(false, !m_enabled)) {
        return;
    }

    m_offset_x = (x - m_x) * m_sensitivity;
    m_offset_y = (m_y - y) * m_sensitivity;
    m_x        = x;
    m_y        = y;
}

std::pair<float, float> MouseManager::position() {
    return {m_x, m_y};
}

std::pair<float, float> MouseManager::offset() {
    return {m_offset_x, m_offset_y};
}


void MouseManager::showUI() {
    constexpr std::string_view name = "Mouse";

    if (ImGui::ScopedBeginMainMenuBar()) {
        if (ImGui::ScopedBeginMenu("Debug")) {
            ImGui::MenuItem(name.data(), nullptr, &RENDER.windowState(name));
        }
    }

    if (!RENDER.windowState(name)) {
        return;
    }

    if (ImGui::ScopedBegin(name.data(), &RENDER.windowState(name), ImGuiWindowFlags_NoCollapse)) {
        ImGui::SliderFloat("Sensitive", &m_sensitivity, 0.01F, 0.5F);
        ImGui::Text("This is pressed key.");
        for (size_t i = 0, size = m_buttons.size(); i < size; ++i) {
            if (m_buttons[i]) {
                ImGui::Text("KeyCode = %ti", i);
            }
        }

        ImGui::Text("Mouse coords: %f %f", m_x, m_y);
    }
}
