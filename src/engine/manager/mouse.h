#pragma once


struct MouseManager final {
    MouseManager();
    void setButton(size_t code, bool state);
    bool button(size_t code);
    void reset();
    void setEnabled(bool value);

    void                    setPosition(float x, float y);
    std::pair<float, float> position();
    std::pair<float, float> offset();

private:
    void showUI();

private:
    std::array<bool, GLFW_MOUSE_BUTTON_LAST + 1> m_buttons     = {};
    float                                        m_x           = 0;
    float                                        m_y           = 0;
    float                                        m_offset_x    = 0;
    float                                        m_offset_y    = 0;
    float                                        m_sensitivity = 0.1F;
    bool                                         m_enabled     = true;
};

#define MOUSE_MANAGER Singleton<MouseManager>::instance()
