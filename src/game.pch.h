#pragma once

// IWYU pragma: begin_exports

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <simple-ecs/ECS.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>
#include <imgui.h>
#include <glm/gtx/hash.hpp>
#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <memory>
#include <mutex>
#include <queue>
#include <render/core/keep_alive_guard.h>
#include <render/render.h>
#include <set>
#include <singleton.h>
#include <string>
#include <string_view>
#include <tasks/tasks.h>
#include <thread_access_guard/thread_access_guard.h>
#include <tmp_buffer/tmp_buffer.h>
#include <type_traits>
#include <utility>

#include "common/deffered_call.h"

// IWYU pragma: end_exports


#if TRACY_ENABLE
#define PROFILE_ONLY(...) __VA_ARGS__
#else
#define PROFILE_ONLY(...)
#endif


struct CameraSystem;
struct EntityDebugSystem;
struct ModelLoaderSystem;
struct SaveLoadDebugSystem;


template<GLenum Type>
struct BufferGL;

template<GLenum Type>
struct TextureGL;

template<typename Type>
struct CountedGLObject;

struct MeshGL;
struct ModelGL;
struct ShaderGL;

using ArrayBuffer   = BufferGL<GL_ARRAY_BUFFER>;
using ElementBuffer = BufferGL<GL_ELEMENT_ARRAY_BUFFER>;
using FrameBuffer   = BufferGL<GL_FRAMEBUFFER>;
using RenderBuffer  = BufferGL<GL_RENDERBUFFER>;
using UniformBuffer = BufferGL<GL_UNIFORM_BUFFER>;

using Texture2D   = CountedGLObject<TextureGL<GL_TEXTURE_2D>>;
using TextureCube = CountedGLObject<TextureGL<GL_TEXTURE_CUBE_MAP>>;

using Shader = CountedGLObject<ShaderGL>;
using Mesh   = MeshGL;

using Model = std::shared_ptr<ModelGL>;

using RENDER_TYPE = struct GameRender;
