#include "engine/manager/holder.h"
#include <engine/render/game_render.h>
#include <engine/system/main.h>
#include <engine/system/save_load.h>
#include <simple-ecs/registry.h>
#include <simple-ecs/world.h>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>
#include <tracy/Tracy.hpp>

// clang-format off
#if _WIN32
#   if _DEBUG
#       include <crtdbg.h>
#   else
#       pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup") // hide console window
#   endif
#endif
// clang-format on


int main(int /*unused*/, char** /*unused*/) {
#if _WIN32 && _DEBUG
    std::ignore = _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    spdlog::stopwatch sw;

    // construct render part
    auto managers = std::make_unique<SingletonsHolder>();

    RENDER_LOGGER("Game").addSink<spdlog::sinks::stdout_color_sink_mt>().init().setDefault();
    spdlog::set_level(ECS_DEBUG_RELEASE_SWITCH(spdlog::level::debug, spdlog::level::info));


    World world;
    auto& reg = *world.getRegistry();
    reg.addSystem<MainEngineSystem>(world);
    reg.initNewSystems();

    auto& save_load_system = *reg.getSystem<SaveLoadDebugSystem>();
    save_load_system.load("quick");

    RENDER.setup({.window_title = "My Game", .style = RenderSettings::Style::CUSTOM, .vsync = true});
    RENDER.initWorld(&world);

    spdlog::info("Total loading time {:.3}s", sw);

    // for tests
    // auto observer = Observer(world);
    // for (int i = 0; i < 1'000'000; i++) {
    //     auto e = observer.create();
    //     e.emplace<Transform>();
    //     e.emplace<Speed>();
    //     e.emplace<Direction>();
    // }

    tasks::start([&reg] {
        tracy::SetThreadName("ECS thread");

        RENDER_KEEP_ALIVE_SCOPE_GUARD;
        reg.prepare();
        while (RENDER.isAlive()) {
            reg.syncWithRender();
            ZoneScopedN("ECS");
            reg.exec();
        }
    });

    RENDER_ADD_WINDOW([] {
        static bool show = false;
        if (ImGui::ScopedBeginMainMenuBar()) {
            if (ImGui::ScopedBeginMenu("Logger")) {
                if (ImGui::MenuItem("Game", nullptr, &show)) {}
            }
        }

        if (show) {
            RENDER_LOGGER("Game").window(show);
        }
    });


    RENDER.exec();

    spdlog::info("Total running time {:.3}s", sw);
    return 0;
}
