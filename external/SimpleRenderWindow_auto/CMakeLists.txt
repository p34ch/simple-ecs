option(RENDER_USE_OPENGL_PRESET "Use OpenGL" ON)
option(RENDER_USE_SDL2_PRESET "Use SDL2 with SDL2 render" OFF)

option(RENDER_USE_GLAD "Use GLAD" OFF)
option(RENDER_USE_GLFW "Use GLFW" OFF)
option(RENDER_USE_SDL2 "Use SDL2" OFF)
option(RENDER_USE_SDL2_RENDER "Use SDL2 render" OFF)

option(RENDER_ENABLE_PROFILER "Enable tracy profiler" ON)
option(RENDER_BUILD_PROFILER_SERVER "Build tracy profiler exe. Can be built with CL only" OFF)

option(TRACY_ENABLE "Enable profiling" ON)
option(TRACY_STATIC "Whether to build Tracy as a static library" ON)
option(TRACY_NO_EXIT "Client executable does not exit until all profile data is sent to server" OFF)


AddExternalPackage(
    SimpleRenderWindow
        v1.6.0
        https://gitlab.com/p34ch-main/simple-render-window.git
        NO
        NO
)
