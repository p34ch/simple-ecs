option(MZ_ENABLE "Enables minizip lib" ON)
option(MZ_ZLIB "Enables ZLIB compression" OFF)
option(MZ_BZIP2 "Enables BZIP2 compression" OFF)
option(MZ_LZMA "Enables LZMA & XZ compression" ON)
option(MZ_ZSTD "Enables ZSTD compression" OFF)

option(SQLITEFS_ENABLE_PROFILER "Enable Tracy profiler library" ON)
option(SQLITEFS_BUILD_PROFILER_SERVER "Build Tracy profiler exe. Can be built with CL only" OFF)
option(SQLITEFS_OVERRIDE_GLOBAL_ALLOCATORS "Override global allocators to track memory in Tracy profiler" OFF)

option(TRACY_ENABLE "Enable profiling" ON)
option(TRACY_STATIC "Whether to build Tracy as a static library" ON)
option(TRACY_NO_EXIT "Client executable does not exit until all profile data is sent to server" OFF)

# AES128 AES256 CHACHA20 SQLCIPHER RC4 ASCON128
set(CODEC_TYPE AES256 CACHE STRING "Set default codec type")


AddExternalPackage(
    SQLiteFS
        v1.1.11
        https://gitlab.com/p34ch-main/sqlitefs.git
        NO
        NO
)